package at.plaz.flat_crawler;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Georg Plaz.
 */
public class Value<A> implements Property<A>, Serializable{
    private A value;
    private Bound bound;
    private Set<ValueChangedListener<A>> valueChangedListeners = new LinkedHashSet<>();

    public Value(){

    }

    public Value(A initial){
        set(initial);
    }

    public A get() {
        return value;
    }

    public void set(A value) {
        if(this.value != value && (value == null || !value.equals(this.value))) {
            A oldValue = this.value;
            this.value = value;
            for (ValueChangedListener<A> valueChangedListener : valueChangedListeners) {
                valueChangedListener.valueChanged(oldValue, value);
            }
        }
    }

    public boolean has(){
        return value != null;
    }

    public void addValueListener(ValueChangedListener<A> valueChangedListener){
        valueChangedListeners.add(valueChangedListener);
    }

    public void removeValueListener(ValueChangedListener<A> valueChangedListener){
        valueChangedListeners.remove(valueChangedListener);
    }

    static class IllegalValueChangeException extends Exception {
    }

    public void bind(Value<A> other) throws AlreadyBoundException {
        if(bound != null || other.bound != null){
            throw new AlreadyBoundException();
        }
        new Bound<>(this, other);
    }

    public void clear(){
        set(null);
    }

    public void unbind(){
        if(bound != null){
            bound.kill();
        }
    }

    public boolean isBound() {
        return bound != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Value)) return false;

        Value<?> value1 = (Value<?>) o;

        return value != null ? value.equals(value1.value) : value1.value == null;

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    private static class Bound<A> {
        private Value<A> first;
        private Value<A> second;
        private ValueChangedListener<A> firstListener;
        private ValueChangedListener<A> secondListener;

        public Bound(Value<A> first, Value<A> second){
            if(!first.has()){
                first.set(second.get());
            }else if(!second.has()){
                second.set(first.get());
            }else{
                first.set(second.get());
            }
            this.first = first;
            this.second = second;
            first.bound = this;
            second.bound = this;
            firstListener = (oldValue, newValue) -> second.set(newValue);
            first.addValueListener(firstListener);
            secondListener = (oldValue, newValue) -> first.set(newValue);
            second.addValueListener(secondListener);
        }

        public void kill(){
            first.bound = null;
            second.bound = null;
            first.removeValueListener(firstListener);
            second.removeValueListener(secondListener);
        }
    }

    @Override
    public String toString() {
        return "Value{" +
                value +
                '}';
    }
}
