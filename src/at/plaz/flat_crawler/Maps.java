package at.plaz.flat_crawler;

import com.google.appengine.repackaged.com.google.common.collect.HashBasedTable;
import com.google.appengine.repackaged.com.google.common.collect.Table;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.NotFoundException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.Duration;
import com.google.maps.model.TravelMode;
import com.google.maps.model.Unit;
import org.joda.time.format.ISODateTimeFormat;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Georg Plaz.
 */
public class Maps {
    private static final String FROM_TAG = "from";
    private static final String TO_TAG = "to";
    private static final String TRAVEL_INFO_TAG = "travel_info";
    private static final String VALUE_ATTRIBUTE = "value";
    private static final String TRAVEL_MODE_ATTR = "TravelMode";
    private static final String DATE_ATTR = "date";
    private static final String DURATION_ATTR = "duration";
    private static final String DURATION_HUMAN_READABLE_ATTR = "duration_human_readable";
    public static final String ROOT_TAG = "maps";
    private static Table<String, String, Map<TravelMode, TravelNode>> travelTimes = HashBasedTable.create();
    private static File mapsFile = new File("maps.xml");
    public static final GeoApiContext CONTEXT = new GeoApiContext().setApiKey("AIzaSyBm9FoXel-_upn6VQwSykcWAlIT4plfUjE");
    static{
        read();
    }

    private static void read() {
        if(mapsFile.exists()) {
            try {
                Document document = XmlUtil.readDocument(mapsFile);
                Element rootElement = document.getDocumentElement();
                processFrom(rootElement.getElementsByTagName(FROM_TAG));
            } catch (NoXmlException e) {
                e.printStackTrace();
            } catch (IllegalXmlException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void processTravel(String from, String to, NodeList travelNodeList){
        for (int k = 0; k < travelNodeList.getLength(); k++) {
            Node travelNode = travelNodeList.item(k);
            if (travelNode instanceof Element) {
                Element travel = (Element) travelNode;
                Duration duration = new Duration();
                duration.inSeconds = Long.parseLong(travel.getAttribute(DURATION_ATTR));
                duration.humanReadable = travel.getAttribute(DURATION_HUMAN_READABLE_ATTR);
                String date = travel.getAttribute(DATE_ATTR);
                TravelMode travelMode = TravelMode.valueOf(travel.getAttribute(TRAVEL_MODE_ATTR));
                TravelNode toStore = new TravelNode(duration, travelMode, date);
//                System.out.println("storing.. " + from+ ", "+ to + ": " + toStore);
                store(from, to, toStore, false);
            }
        }
    }

    private static void processTo(String from, NodeList toNodeList){
        for (int j = 0; j < toNodeList.getLength(); j++) {
            Node toNode = toNodeList.item(j);
            if (toNode instanceof Element) {
                Element to = (Element) toNode;
                processTravel(from, to.getAttribute(VALUE_ATTRIBUTE), to.getElementsByTagName(TRAVEL_INFO_TAG));
            }
        }
    }

    private static void processFrom(NodeList fromNodeList){
        for (int i = 0; i < fromNodeList.getLength(); i++) {
            Node fromNode = fromNodeList.item(i);
            if (fromNode instanceof Element) {
                Element from = (Element) fromNode;
                processTo(from.getAttribute(VALUE_ATTRIBUTE), from.getElementsByTagName(TO_TAG));
            }
        }
    }

    public static void main(String... args) {
        String fromAddress = "81673 München Berg am Laim";
        String toAddress = "xyz";
        TravelNode travelNode1 = getTravelTime(fromAddress, "some address", TravelMode.TRANSIT);
        TravelNode travelNode2 = getTravelTime(fromAddress, "some other address", TravelMode.TRANSIT);
        TravelNode travelNode3_0 = getTravelTime("some starting point", "some address", TravelMode.TRANSIT);
        TravelNode travelNode3_1 = getTravelTime("some starting point", "some address", TravelMode.BICYCLING);
        TravelNode travelNode4 = getTravelTime("some starting point", fromAddress, TravelMode.TRANSIT);
        TravelNode travelNode5 = getTravelTime("foo", "bar", TravelMode.BICYCLING);
    }

    public static TravelNode getTravelTime(String from, String to, TravelMode travelMode){
        if(!has(from, to, travelMode)) {
            try {
                String date = "2016-10-26T09:00:00Z";
//                Duration duration = new Duration();
//                duration.inSeconds = Math.round (60 * Math.random() * 15);
//                duration.humanReadable = String.valueOf(duration.inSeconds/60.)+" mins";
//                store(from, to, new TravelNode(duration, travelMode, date), true);

                DirectionsResult result = DirectionsApi.newRequest(CONTEXT)
                        .origin(from)
                        .destination(to)
                        .region("de")
                        .units(Unit.METRIC)
                        .mode(travelMode)
                        .departureTime(ISODateTimeFormat.dateTimeNoMillis().parseDateTime(date))
                        .await();
                if (!(result.routes.length > 0 && result.routes[0].legs.length > 0)) {
                    return null;
                }
                Duration duration = result.routes[0].legs[0].duration;
                if (duration != null) {
                    TravelNode travelNode = new TravelNode(duration, travelMode, date);
                    store(from, to, travelNode, true);
                    return travelNode;
                }
                return null;
            } catch (NotFoundException e){
                System.err.println("Could not do the travel time lookup, because couldn't find either from=\""+from+"\" or to=\""+to+"\".");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return get(from, to, travelMode);
    }

    private static void store(String from, String to, TravelNode travelNode, boolean persist){
        if(!travelTimes.contains(from, to)){
            travelTimes.put(from, to, new HashMap<>());
        }
        travelTimes.get(from, to).put(travelNode.getTravelMode(), travelNode);
        if(persist){
            System.out.println("persisting... from: "+from+", to: "+to+", info: "+travelNode);
            persist();
        }
    }

    private static boolean has(String from, String to, TravelMode travelMode){
        return travelTimes.contains(from, to) && travelTimes.get(from, to).containsKey(travelMode);
    }

    private static TravelNode get(String from, String to, TravelMode travelMode){
        return travelTimes.get(from, to).get(travelMode);
    }

    private static void persist(){
        Document doc = null;
        try {
            doc = XmlUtil.createDocument();
            Element rootElement = doc.createElement(ROOT_TAG);
            doc.appendChild(rootElement);
            for (String rowKey : travelTimes.rowKeySet()){
                Element rowElement = doc.createElement(FROM_TAG);
                rowElement.setAttribute(VALUE_ATTRIBUTE, rowKey);
                rootElement.appendChild(rowElement);
                for(String colKey : travelTimes.row(rowKey).keySet()){
                    Element colElement = doc.createElement(TO_TAG);
                    colElement.setAttribute(VALUE_ATTRIBUTE, colKey);
                    rowElement.appendChild(colElement);
                    for(Map.Entry<TravelMode, TravelNode> travelNodeEntry : travelTimes.get(rowKey, colKey).entrySet()){
                        TravelNode travelNode = travelNodeEntry.getValue();
                        Element element = doc.createElement(TRAVEL_INFO_TAG);
                        element.setAttribute(TRAVEL_MODE_ATTR, travelNode.getTravelMode().name());
                        element.setAttribute(DATE_ATTR, travelNode.getDate());
                        element.setAttribute(DURATION_ATTR, String.valueOf(travelNode.getDuration().inSeconds));
                        element.setAttribute(DURATION_HUMAN_READABLE_ATTR, travelNode.getDuration().humanReadable);
                        colElement.appendChild(element);
                    }
                }
            }
            try {
                XmlUtil.persist(doc, mapsFile);
                System.out.println("persisted maps.xml");
            } catch (PersistException e1) {
                System.err.println("could not persist maps.xml. Error: "+e1.getMessage());
                e1.printStackTrace();
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static class TravelNode{
        private Duration duration;
        private TravelMode travelMode;
        private String date;

        public TravelNode(Duration duration, TravelMode travelMode, String date) {
            this.duration = duration;
            this.travelMode = travelMode;
            this.date = date;
        }

        public Duration getDuration() {
            return duration;
        }

        public TravelMode getTravelMode() {
            return travelMode;
        }

        public String getDate() {
            return date;
        }

        @Override
        public String toString() {
            return "TravelNode{" +
                    "duration=" + duration.humanReadable +
                    ", travelMode=" + travelMode +
                    ", date='" + date + '\'' +
                    '}';
        }
    }
}
