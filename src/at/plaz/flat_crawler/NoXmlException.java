package at.plaz.flat_crawler;

import java.io.File;

/**
 * Created by Georg Plaz.
 */
public class NoXmlException extends FlatData.ReadException {
    public NoXmlException(File folder) {
        super("No xml file in folder " + folder.getAbsolutePath());
    }
}
