package at.plaz.flat_crawler;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Font;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Georg Plaz.
 */
public class SimplePaneDisplay extends GridPane implements MessageDisplay {
//    private GridPane grid = new GridPane();
    private final List<Message> allMessages = new LinkedList<>();
    private Map<String, Boolean> tagsDisplayed = new HashMap<>();
    private TextArea textArea = new TextArea();
    private MenuBar menuBar = new MenuBar();
    private Menu view = new Menu("View");
    private Menu tags = new Menu("Tags");

    public SimplePaneDisplay() {
        menuBar.getMenus().add(view);
        view.getItems().add(tags);
        addNewTag(ERROR_TAG, true);
        addNewTag(INFO_TAG, true);
        addNewTag(DEBUG_TAG, false);
        add(menuBar, 0, 0);
        add(textArea, 0, 1);
        textArea.setWrapText(true);
        textArea.setFont(Font.font("monospaced"));
        maxWidthProperty().addListener((observable, oldValue, newValue) -> {
            textArea.setMaxWidth(newValue.doubleValue());
            textArea.setPrefWidth(newValue.doubleValue());
        });
        setStyle("-fx-background-color:red");
        ColumnConstraints columnConstraints = new ColumnConstraints();
        columnConstraints.setFillWidth(true);
        columnConstraints.setHgrow(Priority.ALWAYS);
        getColumnConstraints().add(columnConstraints);
        getRowConstraints().add(new RowConstraints());
        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setFillHeight(true);
        rowConstraints.setVgrow(Priority.ALWAYS);
        getRowConstraints().add(rowConstraints);
//        setFitToWidth(true);
//        setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
//        setContent(grid);
    }

    @Override
    public void display(Message message) {
        synchronized (allMessages) {
            allMessages.add(message);
            String[] messageTags = message.getTags();
            for (int i = 0; i < messageTags.length; i++) {
                if (!tagsDisplayed.containsKey(messageTags[i])) {
                    addNewTag(messageTags[i], true);
                }
            }
            for (int i = 0; i < messageTags.length; i++) {
                if (tagsDisplayed.get(messageTags[i])) {
                    Platform.runLater(() -> textArea.appendText('\n' + message.render()));
                    break;
                }
            }
        }
    }

    private void addNewTag(String tag, boolean selected){
        tagsDisplayed.put(tag, selected);
        CheckMenuItem tagCheck = new CheckMenuItem(tag);
        tags.getItems().add(tagCheck);
        tagCheck.selectedProperty().setValue(selected);
        tagCheck.selectedProperty().addListener((observable, oldValue, newValue) -> {
            tagsDisplayed.put(tag, newValue);
            updateDisplayedText();
        });
    }

    private void updateDisplayedText(){
        StringBuilder builder = new StringBuilder();
        synchronized (allMessages) {
            for (Message message : allMessages) {
                String[] messageTags = message.getTags();
                for (int i = 0; i < messageTags.length; i++) {
                    if (tagsDisplayed.get(messageTags[i])) {
                        builder.append('\n').append(message.render());
                        break;
                    }
                }
            }
            Platform.runLater(() -> textArea.setText(builder.toString()));
        }
    }
}
