package at.plaz.flat_crawler.crawler;

import at.plaz.flat_crawler.*;
import at.plaz.flat_crawler.filters.DateTimeWrapper;
import org.joda.time.DateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;

/**
 * Created by Georg Plaz.
 */
public class IMS24Crawler extends AbstractFlatCrawler {
    public static final String IM_24_ADDRESS = "https://www.immobilienscout24.de";
    public static final String IMS24_ADDRESS = IM_24_ADDRESS + "/Suche/S-T/Wohnung-Miete/Umkreissuche/M_fcnchen/-/113055/2029726/-/-/30/-/-/EURO--1300,00";
    public static final String IMS_24_EXPOSE = IM_24_ADDRESS + "/expose/";

    public IMS24Crawler(MessageDisplay messageDisplay) {
        super(messageDisplay, new HttpFetcher());

    }

    public IMS24Crawler() {
        this(new SystemOutDisplay(SiteType.IMMOBILIEN_SCOUT_24.getShortName()));
    }

    @Override
    public String[] getFlatAddresses(String flatListingsAddress) throws IOException {
        Document flatListingsDocument = getSiteFetcher().fetch(flatListingsAddress);
        Element element = flatListingsDocument.getElementById("resultListItems");

        Set<String> ids = new LinkedHashSet<>();
        for(Element e : element.getElementsByClass("result-list__listing")){
            for(Element article : e.getElementsByTag("article")) {
                if(article.hasAttr("data-obid")) {
                    String siteId = article.attr("data-obid");
                    ids.add(IMS_24_EXPOSE+siteId);
                }
            }
        }
        return ids.toArray(new String[ids.size()]);
    }

    @Override
    public boolean isNewFlat(String flatAddress) {
        String id = extractIdFromAddress(flatAddress);
        return id == null || !containsFlat(id);
    }

    private String extractIdFromAddress(String address){
        String stringToLookUp = IMS_24_EXPOSE.substring(10);
        int index = address.indexOf(stringToLookUp);
        if(index != -1){
            return address.substring(index+stringToLookUp.length());
        }else {
            return null;
        }
    }

    @Override
    public Flat processFlatDocument(String siteAddress) throws IOException {
        Document document = getSiteFetcher().fetch(siteAddress);
        return runWithReattempts(() -> {
            DateTime discoveryDate = DateTime.now();
            String flatLink = document.location();
            String id = extractIdFromAddress(flatLink);
//            Document document = Jsoup.connect(flatSite).get();
            Element content = document.getElementById("is24-content");

            Flat flat = new Flat();
            flat.getFlatData().getDateDiscovered().set(new DateTimeWrapper(discoveryDate));
            flat.getFlatData().getLastEdited().set(new DateTimeWrapper(discoveryDate));
            flat.getFlatData().getSite().set(IMS_24_EXPOSE + id);
            flat.getFlatData().getSiteType().set(SiteType.IMMOBILIEN_SCOUT_24);
            flat.getFlatData().getId().set(id);

            String areaString = content.getElementsByClass("is24qa-flaeche").first().text().trim();
            areaString = areaString.replace("m²", "").trim();
            setDouble(flat.getFlatData().getQuadraticMeter(), areaString);

            String rentColdString = content.getElementsByClass("is24qa-kaltmiete").first().text().trim();
            setPrice(flat.getFlatData().getRentCold(), Util.cleanPrice(rentColdString));

            String totalRentString = content.getElementsByClass("is24qa-gesamtmiete").first().text().trim();
            setPrice(flat.getFlatData().getTotalRent(), Util.cleanPrice(totalRentString));

            String roomCount = content.getElementsByClass("is24qa-zi").first().text().trim();
            setDouble(flat.getFlatData().getRoomCount(), roomCount);

            Element addressBlock = content.getElementsByClass("address-block").first();
            if (addressBlock != null) {
                String address = addressBlock.text()
                        .replace("(zur Karte)", "")
                        .replace("Die vollständige Adresse der Immobilie erhalten Sie vom Anbieter.", "")
                        .trim();
                flat.getFlatData().getAddress().set(address);
            }

            String title = content.getElementById("expose-title").text().trim();
            flat.getFlatData().getTitle().set(title);

            String dateAsString = getContent(content, "is24qa-bezugsfrei-ab");
            if (dateAsString != null) {
                try {
                    DateTime date = Util.cleanDateString(dateAsString);
                    flat.getFlatData().getAvailableFrom().set(new DateTimeWrapper(date));
                } catch (IllegalArgumentException e) {
                    getDisplay().display("Invalid format: \"" + dateAsString + "\"", MessageDisplay.ERROR_TAG);
                }
            }

            set(flat.getFlatData().getDescription(), getContent(content, "is24qa-objektbeschreibung"));

            set(flat.getFlatData().getInventory(), getContent(content, "is24qa-ausstattung"));

            set(flat.getFlatData().getLocationDescription(), getContent(content, "is24qa-lage"));

            set(flat.getFlatData().getOtherInfo(), getContent(content, "is24qa-sonstiges"));

            Element gallery = content.getElementById("gallery-container");

            Element realtorBox = content.getElementById("is24-expose-realtor-box");

            Element ownerCompany = realtorBox.getElementById("is24-expose-realtor-box-company-mame");
            if (ownerCompany != null) {
                flat.getFlatData().getOwnerCompany().set(ownerCompany.text().trim());
            }

            Element ownerContactName = realtorBox.getElementsByClass("font-semibold").first();
            if (ownerContactName != null) {
                flat.getFlatData().getOwnerName().set(ownerContactName.text().trim());
            }

            Element ownerAddress = realtorBox.getElementsByClass("is24-address").first();
            if (ownerAddress != null) {
                flat.getFlatData().getOwnerAddress().set(ownerAddress.text().trim());
            }

            Element ownerPhoneNumber = realtorBox.getElementsByClass("is24-phone-number").first();
            if (ownerPhoneNumber != null) {
                flat.getFlatData().getOwnerNumber().set(ownerPhoneNumber.text().trim());
            }

            if (gallery != null) {
                final int[] i = {0};
//                File imageFolder = getImageDirectory(id);
//                if (imageFolder.exists() || imageFolder.mkdirs()) {
                List<String> imageAddresses = new LinkedList<>();
                Elements imageElements = gallery.getElementsByClass("sp-image");
                getDisplay().display("found " + imageElements.size() + " images to persist", MessageDisplay.DEBUG_TAG);
                for (Element element : imageElements) {
                    String imageUrl = element.attr("data-src");
                    imageAddresses.add(imageUrl);
                }
                persistImages(id, imageAddresses);
            }
            return flat;
        });
    }

    public SiteType getSiteType(){
        return SiteType.IMMOBILIEN_SCOUT_24;
    }

    private void setDouble(Value<Double> value, String s){
        if(s != null) {
            try {
                double v = Util.parseGermanNumberString(s);
                value.set(v);
            } catch (NumberFormatException e) {
                getDisplay().display("Could not parse to float: \"" + s + "\"", MessageDisplay.ERROR_TAG);
            }
        }
    }

    private void setPrice(Value<Double> value, String s){
        if(s != null) {
            try {
                double v = Util.parseGermanNumberString(s);
                if(v > 0){
                    value.set(v);
                }else{
                    getDisplay().display("Price was 0..", MessageDisplay.DEBUG_TAG);
                }
            } catch (NumberFormatException e) {
                getDisplay().display("Could not parse price to float: \"" + s + "\"", MessageDisplay.ERROR_TAG);
            }
        }
    }

    private static <A> void set(Value<A> value, A s){
        if(s != null) {
            value.set(s);
        }
    }

    private static String getContent(Element parent, String className){
        Elements bezugsfreiAbElements = parent.getElementsByClass(className);
        if(bezugsfreiAbElements.size() > 0){
            return bezugsfreiAbElements.first().text().trim();
        }
        return null;
    }

    public String[] getFlatListings(){
        List<String> addresses = new LinkedList<>();

        String[] result = runWithReattempts(() -> {
            Document document = Jsoup.connect(IMS24_ADDRESS).get();
            Element pageSelection = document.getElementById("pageSelection");
            for (Element element : pageSelection.getElementsByTag("option")) {
                addresses.add(IM_24_ADDRESS + element.attr("value"));
            }
            return addresses.toArray(new String[addresses.size()]);
        });
        if(result == null){
            return new String[0];
        }else{
            return result;
        }
    }
}
