package at.plaz.flat_crawler.crawler;

import at.plaz.flat_crawler.*;
import at.plaz.flat_crawler.filters.DateTimeWrapper;
import org.apache.commons.lang3.StringEscapeUtils;
import org.joda.time.DateTime;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class SalzUndBrotCrawler extends AbstractFlatCrawler {
    public static final String SALZ_UND_BROT_ADDRESS = "https://salzundbrot.com";
    public static final String SALZ_UND_BROT_LISTINGS_ADDRESS = SALZ_UND_BROT_ADDRESS + "/suche";
    public static final String SALZ_UND_BROT_MUNICH_LISTINGS_ADDRESS = SALZ_UND_BROT_LISTINGS_ADDRESS + "/#!search={\"c\":5}";
    public static final String SALZ_UND_BROT_OBJECT_ADDRESS = SALZ_UND_BROT_LISTINGS_ADDRESS + "/#!object=";
    public static final String OBJECT_IDENTIFIER = "object=";
    private PhantomFetcher phantomFetcher;

    private SalzUndBrotCrawler(MessageDisplay messageDisplay, PhantomFetcher phantomFetcher) {
        super(messageDisplay, phantomFetcher);
        this.phantomFetcher = phantomFetcher;
    }

    public SalzUndBrotCrawler(MessageDisplay messageDisplay) {
        this(messageDisplay, new PhantomFetcher());
    }

    public SalzUndBrotCrawler() {
        this(new SystemOutDisplay(SiteType.IMMOWELT.getShortName()));
    }

    @Override
    public Flat processFlatDocument(String address) throws IOException {
        DateTime now = DateTime.now();

        Document document = phantomFetcher.fetch(address, ExpectedConditions.presenceOfElementLocated(By.id("object-details")));

        String siteAddress = document.location();
        String id = toId(siteAddress);

        Flat flat = new Flat();
        FlatData flatData = flat.getFlatData();

        flatData.getId().set(id);
        flatData.getSite().set(siteAddress);
        flatData.getSiteType().set(getSiteType());
        flatData.getLastEdited().set(new DateTimeWrapper(now));
        flatData.getDateDiscovered().set(new DateTimeWrapper(now));

        Element objectData = document.getElementById("object-details");
        if(objectData == null){
            System.out.println("wtf?!");
        }
        String data = objectData.attr("data-object");
        flatData.getQuadraticMeter().set(Util.saveIsolateDouble(data, "\"size\":\"", "\"", getDisplay()));
        flatData.getRoomCount().set(Util.saveIsolateDouble(data, "\"rooms\":\"", "\"", getDisplay()));
        flatData.getRentCold().set(Util.saveIsolateDouble(data, "\"fee\":\"", "\"", getDisplay()));
        flatData.getSideCost().set(Util.saveIsolateDouble(data, "\"expenses\":\"", "\"", getDisplay()));
        flatData.getDeposit().set(Util.saveIsolateDouble(data, "\"deposit\":\"", "\"", getDisplay()));
        if(flatData.getSideCost().has() && flatData.getRentCold().has()){
            flatData.getTotalRent().set(flatData.getSideCost().get() + flatData.getRentCold().get());
        }
        String street = StringEscapeUtils.unescapeJava(Util.saveIsolate(data, "\"street\":\"", "\"", getDisplay())).trim();
        String zip = StringEscapeUtils.unescapeJava(Util.saveIsolate(data, "\"postalcode\":\"", "\"", getDisplay())).trim();
        String city = StringEscapeUtils.unescapeJava(Util.saveIsolate(data, "\"city\":\"", "\"", getDisplay())).trim();

        for(Element tr : objectData.getElementsByTag("tbody").first().getElementsByTag("tr")){
            Element th = tr.getElementsByTag("th").first();
            Element td = tr.getElementsByTag("td").first();
            if(th != null && td != null) {
                String tdText = td.text().trim();
                switch (th.text().toLowerCase().trim()) {
                    case "erstellungsdatum":
                        try {
                            flatData.getDateCreated().set(new DateTimeWrapper(Util.cleanDateString(tdText)));
                        } catch (IllegalArgumentException e) {

                        }
                        break;
                    case "bezugsdatum":
                        try {
                            flatData.getAvailableFrom().set(new DateTimeWrapper(Util.cleanDateString(tdText)));
                        } catch (IllegalArgumentException e) {

                        }
                        break;
                }
            }
        }
        if(!zip.isEmpty() || !street.isEmpty()){
            StringBuilder builder = new StringBuilder();
            if(!street.isEmpty()){
                builder.append(street).append(", ");
            }
            if(!zip.isEmpty()){
                builder.append(zip).append(", ");
            }
            builder.append(city);
            flatData.getAddress().set(builder.toString());
        }
//        flatData.de().set(Util.saveIsolateDouble(data, "\"deposit\":\"", "\"", getDisplay()));

        System.out.println(flatData);
        return new Flat(flatData);
    }

    @Override
    public SiteType getSiteType() {
        return SiteType.SALZ_UND_BROT;
    }

    @Override
    public String[] getFlatAddresses(String flatListingsAddress) {
        Document flatListingsDocument = phantomFetcher.fetch(flatListingsAddress, ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"main-searchresults\"]/div/ul[2]")));
        Element mainSearchResults = flatListingsDocument.getElementById("main-searchresults");
        List<String> addresses = new LinkedList<>();
        for(Element li : mainSearchResults.getElementsByTag("li")){
            String id = li.attr("data-id").trim();
            if(!id.isEmpty()){
                addresses.add(SALZ_UND_BROT_OBJECT_ADDRESS + id);
            }
        }
        return addresses.toArray(new String[addresses.size()]);
    }

    @Override
    public String[] getFlatListings() {
        phantomFetcher.deleteCookies();
        return new String[]{SALZ_UND_BROT_MUNICH_LISTINGS_ADDRESS};
    }

    @Override
    public boolean isNewFlat(String flatAddress) {
        String id = toId(flatAddress);
        return id == null || !containsFlat(id);
    }

    private String toId(String flatAddress) {
        int startIndex = flatAddress.indexOf(OBJECT_IDENTIFIER) + OBJECT_IDENTIFIER.length();

        int endIndex = flatAddress.indexOf("&", startIndex);
        if(endIndex == -1){
            return flatAddress.substring(startIndex);
        }else{
            return flatAddress.substring(startIndex, endIndex);
        }
    }
}
