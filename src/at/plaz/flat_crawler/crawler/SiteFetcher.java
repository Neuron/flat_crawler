package at.plaz.flat_crawler.crawler;

import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by Georg Plaz.
 */
public interface SiteFetcher {
    Document fetch(String address) throws IOException;
}