package at.plaz.flat_crawler.crawler;

import java.io.IOException;

/**
 * Created by Georg Plaz.
 */
public interface ConnectionRunnable<A> {
    A run() throws IOException;
}
