package at.plaz.flat_crawler.crawler;

import at.plaz.flat_crawler.*;
import at.plaz.flat_crawler.filters.FlatFilter;
import at.plaz.flat_crawler.ui.FilterPane;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.*;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.*;

import static at.plaz.flat_crawler.MessageDisplay.*;
import static at.plaz.flat_crawler.MessageDisplay.ERROR_TAG;

/**
 * Created by Georg Plaz.
 */
public abstract class AbstractFlatCrawler implements FlatCrawler {
    public static final int SLEEP_EPSILON = 100;
    public static final int MAX_RETRY_DELAY = 60000;
    public static final File SITE_DETAILS_FOLDER = new File("flat_data");
    private MessageDisplay messageDisplay;
    private final List<FlatFilter> flatFilters = new LinkedList<>();
    private List<FlatFoundListener> flatFoundListeners = new LinkedList<>();
    private ListeningThread thread;
    private boolean isRunning = false;
    private Map<String, Flat> flats = new LinkedHashMap<>();
    private Value<Float> refreshProgress = new Value<>();
    private Value<Long> refreshDelay = new Value<>(60L * 1000);
    private SiteFetcher siteFetcher;

    public AbstractFlatCrawler(MessageDisplay messageDisplay, SiteFetcher siteFetcher) {
        this.messageDisplay = messageDisplay;
        this.siteFetcher = siteFetcher;
        File dir = getDirectory();
        File[] subFiles = dir.listFiles();
        if(subFiles != null) {
            for (File folder : subFiles) {
                try {
                    Flat flat = Flat.read(folder);
                    flats.put(folder.getName(), flat);
                } catch (NoXmlException e) {
                    messageDisplay.display("Found corrupt entry (xml file missing). Deleting folder " + folder.getAbsolutePath(), ERROR_TAG);
                    try {
                        Util.deleteFileOrFolder(folder.toPath());
                        messageDisplay.display("Successfully deleted faulty folder!", DEBUG_TAG);
                    } catch (IOException e1) {
                        messageDisplay.display("Could not delete folder. Error: " + e.getMessage(), ERROR_TAG);
                    }
                } catch (FlatData.ReadException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void setMessageDisplay(MessageDisplay messageDisplay) {
        this.messageDisplay = messageDisplay;
    }

    @Override
    public Value<Long> getRefreshDelay() {
        return refreshDelay;
    }

    @Override
    public void start(){
        if(thread == null) {
            thread = new ListeningThread();
            thread.start();
        }
    }

    @Override
    public void addListener(FlatFoundListener flatFoundListener) {
        flatFoundListeners.add(flatFoundListener);
    }

    @Override
    public void removeListener(FlatFoundListener flatFoundListener) {
        flatFoundListeners.remove(flatFoundListener);
    }

    @Override
    public void addFilter(FlatFilter flatFilter) {
        synchronized (flatFilters) {
            messageDisplay.display("added filter: "+flatFilter, DEBUG_TAG);
            flatFilters.add(flatFilter);
        }
    }

    @Override
    public void removeFilter(FlatFilter flatFilter) {
        synchronized (flatFilters) {
            messageDisplay.display("removed filter: "+flatFilter, DEBUG_TAG);
            flatFilters.remove(flatFilter);
        }
    }

    public <A> A runWithReattempts(ConnectionRunnable<A> runnable) {
        return runWithReattempts(runnable, 10);
    }

    public <A> A runWithReattempts(ConnectionRunnable<A> runnable, int connectionAttempts) {
        int triesLeft = connectionAttempts;
        while (triesLeft-- > 0) {
            String errorAppendix = triesLeft + " tries left. Trying again..";
            try {
                return runnable.run();
            } catch (UnknownHostException e) {
                messageDisplay.display("Could not reach host. Please check your connection! " + errorAppendix, ERROR_TAG);
            } catch (SocketTimeoutException e) {
                messageDisplay.display("Socket timed out.. " + errorAppendix, ERROR_TAG);
            } catch (IOException e) {
                messageDisplay.display("Error connecting: " + e.getMessage()+" " + errorAppendix, ERROR_TAG);
            } catch (Exception e) {
                messageDisplay.display("Error while fetching addresses.. Error: " + e.getMessage() + " " + errorAppendix, ERROR_TAG);
                e.printStackTrace();
            }
            if(triesLeft > 0){
                long delay = (long) (1000 * Math.pow(1.6, connectionAttempts - triesLeft));
                messageDisplay.sleepAndPrint(Math.min(MAX_RETRY_DELAY, delay));
            }else{
                messageDisplay.display("No tries left. Skipping this task..", ERROR_TAG);
            }
        }
        return null;
    }

    public void persistImages(String id, List<String> addresses){
        int i = 0;
        File imageFolder = getImageDirectory(id);
        if (imageFolder.exists() || imageFolder.mkdirs()) {
            for (String imageUrl : addresses) {
                final int finalI = i++;
                runWithReattempts(() -> {
                    Connection.Response resultImageResponse = Jsoup.connect(imageUrl)
                            .ignoreContentType(true).execute();

                    FileOutputStream out = (new FileOutputStream(new File(imageFolder, ("img" + finalI) + ".jpg")));
                    out.write(resultImageResponse.bodyAsBytes());  // resultImageResponse.body() is where the image's contents are.
                    out.close();
                    return null;
                });
            }
        } else {
            getDisplay().display("Failed to persist images, because folder " + imageFolder +
                    " could not be created.", ERROR_TAG);
        }
    }

    public SiteFetcher getSiteFetcher() {
        return siteFetcher;
    }

    public abstract Flat processFlatDocument(String address) throws IOException;

    public abstract SiteType getSiteType();

    @Override
    public Value<Float> getRefreshProgress() {
        return refreshProgress;
    }

    @Override
    public void stop() {
        isRunning = false;
        thread.interrupt();
    }

    @Override
    public Collection<Flat> getFlats() {
        return flats.values();
    }

    private class ListeningThread extends Thread{
        @Override
        public void run() {
            isRunning = true;
            while (isRunning) {
                try {
                    messageDisplay.display("Fetching sites..", INFO_TAG);
                    String[] flatListings = getFlatListings();
                    messageDisplay.display("Found " + flatListings.length + " pages with sites!", INFO_TAG);
                    for (String flatListing : flatListings) {
                        refreshProgress.set(-1f);
                        messageDisplay.display("Searching for flats on page: " + flatListing, DEBUG_TAG);
                        runWithReattempts(() -> {
//                            Document document = siteFetcher.fetch(flatListing);
                            String[] flatAddresses = getFlatAddresses(flatListing);
                            for (String flatAddress : flatAddresses){
                                if(isNewFlat(flatAddress)){
                                    messageDisplay.display("Found new flat: " + flatAddress, INFO_TAG);
                                    Flat flat = runWithReattempts(() -> processFlatDocument(flatAddress));
                                    foundFlat(flat);
                                }
                            }
                            return null;
                        });
                    }
                    messageDisplay.display("Sleeping for "+(refreshDelay.get()/1000)+" secs.", INFO_TAG);
                    int timeSlept = 0;
                    while (timeSlept <= refreshDelay.get() && isRunning) {
                        timeSlept += SLEEP_EPSILON;
                        try {
                            Thread.sleep(SLEEP_EPSILON);
                        } catch (InterruptedException e) {
                            messageDisplay.display("Woke up prematurely!", ERROR_TAG);
                            isRunning = false;
                        }
                        if (refreshProgress != null) {
                            refreshProgress.set((float) timeSlept / refreshDelay.get());
                        }
                    }
                    messageDisplay.display("Woke up!", INFO_TAG);
                }catch (Exception e){
                    e.printStackTrace();
                    messageDisplay.display("Caught exception. trying to proceed after sleep. Error: " + e.getMessage(), ERROR_TAG);
                    messageDisplay.sleepAndPrint(refreshDelay.get() * 10);
                }
            }
        }
    }

    public abstract String[] getFlatAddresses(String flatListingsAddress) throws IOException;

    public abstract String[] getFlatListings();

    public abstract boolean isNewFlat(String flatAddress);

    public File getDirectory() {
        return new File(SITE_DETAILS_FOLDER, getSiteType().getShortName());
    }

    public File getImageDirectory(String flatId) {
        return new File(new File(getDirectory(), flatId), "images");
    }

    public boolean containsFlat(String id){
        return flats.containsKey(id);
    }

    public boolean foundFlat(Flat newFlat){
        try {
            newFlat.persist();
        } catch (PersistException e) {
            getDisplay().display("Could not persist flat. Error: "+e.getMessage(), ERROR_TAG);
        }
        flats.put(newFlat.getFlatData().getId().get(), newFlat);

        synchronized (flatFilters) {
            for (FlatFilter flatFilter : flatFilters) {
                if (!flatFilter.isValid(newFlat)) {
                    messageDisplay.display("Flat did not pass filter: " + flatFilter.toReadableString(), INFO_TAG);
                    return false;
                }
            }
        }
        for (FlatFoundListener flatFoundListener : flatFoundListeners){
            flatFoundListener.found(newFlat);
        }
        return true;
    }

    public MessageDisplay getDisplay() {
        return messageDisplay;
    }

}
