package at.plaz.flat_crawler.crawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.net.UrlChecker;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Created by Georg Plaz.
 */
public class PhantomFetcher implements SiteFetcher {
    private static DesiredCapabilities caps;
    private final Object driverLock = new Object();
    private WebDriver driver;
    static{
        caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true); // enabled by default
        caps.setCapability(
                PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                "res/phantomjs-2.1.1-windows/bin/phantomjs.exe"
        );

        caps.setCapability(PhantomJSDriverService.PHANTOMJS_GHOSTDRIVER_CLI_ARGS, "--loglevel=NONE");
    }

    public PhantomFetcher() {

    }

    private WebDriver getDriver(){
        synchronized (driverLock) {
            if (driver == null) {
                driver = new PhantomJSDriver(caps);
                Enumeration<String> loggerNames = LogManager.getLogManager().getLoggerNames();
                while (loggerNames.hasMoreElements()) {
                    String loggerName = loggerNames.nextElement();
                    if (loggerName.startsWith("org.openqa.selenium")) {
                        Logger.getLogger(loggerName).setLevel(Level.OFF);
                    }
                }
            }
            return driver;
        }
    }

    @Override
    public Document fetch(String address) {
        synchronized (driverLock) {
            getDriver().get(address);
            return Jsoup.parse(driver.getPageSource(), address);
        }
    }

    public Document fetch(String address, ExpectedCondition<?> expectedCondition) {
        synchronized (driverLock) {
            getDriver().get(address);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            new WebDriverWait(driver, 60).until(expectedCondition);
            return Jsoup.parse(driver.getPageSource(), address);
        }
    }

    public <A> A waitForCondition(ExpectedCondition<A> condition){
        return new WebDriverWait(getDriver(), 10).until(condition);
    }

    public void deleteCookies(){
        getDriver().manage().deleteAllCookies();
    }
}
