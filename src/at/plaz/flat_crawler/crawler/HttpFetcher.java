package at.plaz.flat_crawler.crawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by Georg Plaz.
 */
public class HttpFetcher implements SiteFetcher {
    @Override
    public Document fetch(String address) throws IOException {
        return Jsoup.connect(address).get();
    }
}
