package at.plaz.flat_crawler.crawler;

import at.plaz.flat_crawler.*;
import at.plaz.flat_crawler.filters.DateTimeWrapper;
import org.joda.time.DateTime;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.*;

import static at.plaz.flat_crawler.MessageDisplay.DEBUG_TAG;
import static at.plaz.flat_crawler.MessageDisplay.ERROR_TAG;

/**
 * Created by Georg Plaz.
 */
public class ImmoweltCrawler extends AbstractFlatCrawler {
    public static final String IMMOWELT_ADDRESS = "https://www.immowelt.de";
    public static final String IMMOWELT_LISTINGS_ADDRESS = IMMOWELT_ADDRESS + "/liste/muenchen/wohnungen/mieten?primi=400&prima=1200&sort=relevanz";
    public static final String PAGE_WITHOUT_INDEX = IMMOWELT_LISTINGS_ADDRESS + "&cp=";
    private PhantomFetcher phantomFetcher;

    private ImmoweltCrawler(MessageDisplay messageDisplay, PhantomFetcher phantomFetcher) {
        super(messageDisplay, phantomFetcher);
        this.phantomFetcher = phantomFetcher;
    }

    public ImmoweltCrawler(MessageDisplay messageDisplay) {
        this(messageDisplay, new PhantomFetcher());
    }

    public ImmoweltCrawler() {
        this(new SystemOutDisplay(SiteType.IMMOWELT.getShortName()));
    }

    @Override
    public Flat processFlatDocument(String url) {
        DateTime now = DateTime.now();
        Document document = phantomFetcher.fetch(url);
        String siteAddress = document.location();
        String id = toId(siteAddress);

        Flat flat = new Flat();
        FlatData flatData = flat.getFlatData();

        flatData.getId().set(id);
        flatData.getSite().set(siteAddress);
        flatData.getSiteType().set(getSiteType());
        flatData.getLastEdited().set(new DateTimeWrapper(now));
        flatData.getDateDiscovered().set(new DateTimeWrapper(now));

        String html = document.html();

        String scriptBit = Util.isolate(html, "<body class=\"modern_browser \">", "</script>\n");

        Integer photoCountInt = Util.saveIsolateInt(scriptBit, "\"object_count_photos\":", ",\"", getDisplay());
        int photoCount = -1;
        if(photoCountInt != null){
//            ExpectedCondition expectedCondition = ExpectedConditions.presenceOfElementLocated(By.cssSelector("#imagesArea > div:nth-child("+photoCount+") > img"));
//            getDisplay().display("Waiting for img obj to appear..", DEBUG_TAG);
//            phantomFetcher.waitForCondition(expectedCondition);
            photoCount = photoCountInt;
            getDisplay().display("Determined that flat has "+photoCount+" photos!", DEBUG_TAG);
        }else{
            getDisplay().display("Could not determine how many photos "+ Util.quote(id)+" has. Therefore no photos have been saved!", ERROR_TAG);
        }

        set(flatData.getRentCold(), Util.saveIsolateDouble(scriptBit, "\"object_price\":",  ",\"", getDisplay()));
        set(flatData.getRoomCount(), Util.saveIsolateDouble(scriptBit, ",\"object_rooms\":", ",\"", getDisplay()));
        set(flatData.getQuadraticMeter(), Util.saveIsolateDouble(scriptBit, ",\"object_area\":", ",\"", getDisplay()));

        Element location = document.getElementsByClass("location").first();
        Element noS = location.getElementsByClass("no_s").first();
        String address = noS.text().trim();
        set(flatData.getAddress(), address);

        Element divImmobilie = document.getElementById("divImmobilie");
        String freeFromDate = Util.isolate(divImmobilie.text(), "Bezug: ", null);
        try {
            DateTime date = Util.cleanDateString(freeFromDate);
            flat.getFlatData().getAvailableFrom().set(new DateTimeWrapper(date));
        } catch (IllegalArgumentException e) {
            getDisplay().display("Invalid format: " + Util.quote(freeFromDate) +".", MessageDisplay.ERROR_TAG);
        }

        Element priceBit = document.getElementById("divPreise");
        for (Element dataRow : priceBit.getElementsByClass("datarow")){
            String rowText = dataRow.text();
            if(rowText.contains("Warmmiete") && !rowText.contains("in Warmmiete enthalten")){
                setPrice(flatData.getTotalRent(), dataRow.getElementsByClass("datacontent").first());
                break;
            }
        }
        if(photoCount != -1){
            Element imageContainer = document.getElementById("imagesArea");
            if(imageContainer != null) {
                List<String> imageAddresses = new LinkedList<>();
                for (Element image : imageContainer.getElementsByTag("img")) {
                    String imageAddress;
                    if(image.hasAttr("data-src")){
                        imageAddress = image.attr("data-src");
                    }else{
                        imageAddress = image.attr("src");
                    }
                    imageAddresses.add(imageAddress);
                }
                getDisplay().display("Found image addresses: "+imageAddresses, DEBUG_TAG);
                persistImages(id, imageAddresses);
            }
        }
        return flat;
    }

    private void setPrice(Value<Double> value, Element element){
        if(element != null){
            String text = element.text().replaceAll("[€.]", "").replace(",", ".").trim();
            try{
                value.set(Double.valueOf(text));
            }catch (NumberFormatException e){
                getDisplay().display("Could not parse price \""+element.text()+"\" to double.", DEBUG_TAG);
            }
        }else{
            getDisplay().display("Could not set price, because element was null!");
        }
    }

    private <A> void set(Value<A> value, A v){
        if(v != null){
            value.set(v);
        }
    }

    @Override
    public SiteType getSiteType() {
        return SiteType.IMMOWELT;
    }

    @Override
    public String[] getFlatAddresses(String flatListingsAddress) {
        Document flatListingsDocument = phantomFetcher.fetch(flatListingsAddress);
        List<String> flatAddresses = new LinkedList<>();
        Element baseContainer = flatListingsDocument.getElementById("basecontainer");
        Element immoListe = baseContainer.getElementsByClass("immoliste").first();
        Element content = immoListe.getElementsByClass("iw_content").first();
        for(Element element : content.getElementsByClass("js-object")){
            String id = element.attr("data-oid");
            flatAddresses.add(toAddress(id));
        }
        getDisplay().display("Found the following ids on current page: " + flatAddresses + ".", DEBUG_TAG);

        return flatAddresses.toArray(new String[flatAddresses.size()]);
    }

    private String toAddress(String id){
        return IMMOWELT_ADDRESS + "/expose/" + id + "?bc=13";
    }

    private String toId(String address){
        String exposeBit = "/expose/";
        String id = Util.isolate(address, exposeBit, "?");
        return id;
    }

    @Override
    public String[] getFlatListings() {
        phantomFetcher.deleteCookies();
        return runWithReattempts(() -> {
            Document document = getSiteFetcher().fetch(IMMOWELT_LISTINGS_ADDRESS);
            Element pageSelection = document.getElementById("pnlPaging");
            if(pageSelection != null) {
                int max = -1;
                for (Element pageButton : pageSelection.getElementsByClass("btn_01")) {
                    try {
                        int humanReadablePageIndex = Integer.parseInt(pageButton.text().trim());
                        max = Math.max(max, humanReadablePageIndex - 1);
                    } catch (NumberFormatException e) {
                        getDisplay().display("Could not parse \"" + pageButton.text().trim() + "\" to site id..", ERROR_TAG);
                        //ignore
                    }
                }
                if (max > 0) {
                    String[] addresses = new String[max];
                    for (int i = 0; i < max; i++) {
                        addresses[i] = PAGE_WITHOUT_INDEX + (i + 1);
                    }
                    return addresses;
                }
            } else {
                getDisplay().display("Could not fetch element from page. Check connection.", ERROR_TAG);
            }
            return new String[0];
        });
    }

    @Override
    public boolean isNewFlat(String flatAddress) {
        String id = toId(flatAddress);
        return id == null || !containsFlat(id);
    }
}
