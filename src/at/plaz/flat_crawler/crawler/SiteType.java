package at.plaz.flat_crawler.crawler;

/**
 * Created by Georg Plaz.
 */
public enum SiteType {
    IMMOBILIEN_SCOUT_24("ims_24", "Immobilien scout 24"), IMMOWELT("iw", "Immowelt"), SALZ_UND_BROT("SuB", "SALZ & BROT");

    private String shortName;
    private String longName;

    SiteType(String shortName, String longName) {
        this.shortName = shortName;
        this.longName = longName;
    }

    public String getShortName() {
        return shortName;
    }
    public String getLongName() {
        return longName;
    }
}
