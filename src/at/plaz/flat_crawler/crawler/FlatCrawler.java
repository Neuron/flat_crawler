package at.plaz.flat_crawler.crawler;

import at.plaz.flat_crawler.Flat;
import at.plaz.flat_crawler.FlatFoundListener;
import at.plaz.flat_crawler.MessageDisplay;
import at.plaz.flat_crawler.Value;
import at.plaz.flat_crawler.filters.FlatFilter;

import java.util.Collection;

/**
 * Created by Georg Plaz.
 */
public interface FlatCrawler {
    void setMessageDisplay(MessageDisplay messageDisplay);

    Value<Long> getRefreshDelay();

    void start();

    void addListener(FlatFoundListener flatFoundListener);

    void removeListener(FlatFoundListener flatFoundListener);

    void addFilter(FlatFilter flatFilter);

    void removeFilter(FlatFilter flatFilter);

    Value<Float> getRefreshProgress();

    SiteType getSiteType();

    void stop();

    Collection<Flat> getFlats();
}
