package at.plaz.flat_crawler.ui;

import at.plaz.flat_crawler.Value;
import at.plaz.flat_crawler.filters.FlatFilter;
import at.plaz.flat_crawler.filters.OrFilter;

import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class CreateOrFilterPane extends CreateMultiFilterPane {
    public CreateOrFilterPane(Value<FlatFilter> flatFilter){
        super(flatFilter, "First filter", "Second filter");
    }

    @Override
    public FlatFilter render(List<FlatFilter> filters) {
        return new OrFilter(filters.get(0), filters.get(1));
    }
}
