package at.plaz.flat_crawler.ui;

import at.plaz.flat_crawler.Value;
import at.plaz.flat_crawler.filters.FlatFilter;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public abstract class CreateMultiFilterPane extends VBox {
    private final GridPane filterDetails;
    private final Value<FlatFilter> flatFilterValue;
    private final TextField labelField;
    private List<Button> buttons;
    private List<String> labels;
    private List<FlatFilter> flatFilters;
    private List<Label> filterLabels;

    public CreateMultiFilterPane(Value<FlatFilter> flatFilterValue, String... labels){
        this.flatFilterValue = flatFilterValue;
        this.labels = new ArrayList<>();
        this.buttons = new ArrayList<>();
        flatFilters = new ArrayList<>();
        filterLabels = new ArrayList<>();
        setPadding(new Insets(15, 15, 15, 15));
        filterDetails = new GridPane();
        filterDetails.setHgap(10);
        filterDetails.setVgap(10);
        getChildren().add(filterDetails);
        labelField = new TextField();

        filterDetails.add(new Label("Filter label:"), 0, 0);
        filterDetails.add(labelField, 1, 0);

        for (int i = 0; i < labels.length; i++) {
            addFilter(labels[i]);
        }
    }

    public void addFilter(String label){
        final int index = flatFilters.size();
        labels.add(label);
        filterDetails.add(new Label(label), 0, index + 1);
        filterLabels.add(new Label("null"));
        Button createFilterButton = new Button("Create");
        buttons.add(createFilterButton);
        filterDetails.add(createFilterButton, 1, index+1);
        flatFilters.add(null);
        createFilterButton.setOnAction(event -> {
            setFilter(FilterPane.showFlatFilterDialog(labels.get(index), labels.get(index)), index);
        });
    }

    private void setFilter(FlatFilter filter, int index){
        if(filter != null) {
            filterDetails.getChildren().remove(buttons.get(index));
            flatFilters.add(index, filter);
            filterLabels.get(index).setText(flatFilters.get(index).toReadableString());
            filterDetails.add(new Label(filter.toReadableString()), 1, index+1);
            for (int i = 0; i < labels.size(); i++) {
                if (flatFilters.get(index) == null) {
                    return;
                }
            }
            FlatFilter flatFilter = render(flatFilters);
            flatFilter.setLabel(labelField.getText());
            flatFilterValue.set(flatFilter);
        }
    }

    public abstract FlatFilter render(List<FlatFilter> filters);

}
