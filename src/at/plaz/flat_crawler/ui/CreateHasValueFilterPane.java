package at.plaz.flat_crawler.ui;

import at.plaz.flat_crawler.FlatData;
import at.plaz.flat_crawler.Value;
import at.plaz.flat_crawler.filters.*;
import at.plaz.flat_crawler.value_retriever.FlatValueRetriever;
import com.sun.javafx.collections.ObservableListWrapper;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class CreateHasValueFilterPane extends VBox {
    private final GridPane filterDetails;
    private final TextField labelField;

    public CreateHasValueFilterPane(Value<FlatFilter> filterValue) {
        setPadding(new Insets(15, 15, 15, 15));
        filterDetails = new GridPane();
        filterDetails.setHgap(10);
        filterDetails.setVgap(10);
        getChildren().add(filterDetails);
        labelField = new TextField();

        filterDetails.add(new Label("Filter label:"), 0, 0);
        filterDetails.add(labelField, 1, 0);

        List<FlatValueRetriever<?>> flatValueRetrievers = new LinkedList<>(FlatData.getFlatValueRetrievers());
        ComboBox<FlatValueRetriever<?>> comboBox = new ComboBox<>(new ObservableListWrapper<>(flatValueRetrievers));
        comboBox.setConverter(new StringConverter<FlatValueRetriever<?>>() {
            @Override
            public String toString(FlatValueRetriever retriever) {
                return retriever.getLabel();
            }

            @Override
            public FlatValueRetriever<?> fromString(String string) {
                return null;
            }
        });

        comboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            filterValue.set(new HasValueFilter<>(newValue));
        });
        filterDetails.add(new Label("Filter type:"), 0, 1);
        filterDetails.add(comboBox, 1, 1);
    }
}
