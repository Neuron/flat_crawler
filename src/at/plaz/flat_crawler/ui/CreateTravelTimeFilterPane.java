package at.plaz.flat_crawler.ui;

import at.plaz.flat_crawler.FlatData;
import at.plaz.flat_crawler.Value;
import at.plaz.flat_crawler.filters.FlatFilter;
import at.plaz.flat_crawler.value_retriever.TravelTimeRetriever;
import com.google.maps.model.TravelMode;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import static com.google.maps.model.TravelMode.*;

/**
 * Created by Georg Plaz.
 */
public class CreateTravelTimeFilterPane extends CreateMinMaxFilterPane {
    private TextField addressField;
    private int minMaxPos;
    private ComboBox<TravelMode> comboBox;

    public CreateTravelTimeFilterPane(Value<FlatFilter> filterValue) {
        super(filterValue);
    }

    @Override
    public void initContent(GridPane content, int verticalPos) {
        content.add(new Label("Travel destination:"), 0, verticalPos);
        content.add(addressField = new TextField(), 1, verticalPos++);

        content.add(new Label("Method of travel:"), 0, verticalPos);
        content.add(comboBox = new ComboBox<>(FXCollections.observableArrayList(DRIVING, WALKING, BICYCLING, TRANSIT)), 1, verticalPos++);

        ChangeListener<Object> changeListener = (observable, oldValue, newValue) -> initMinMax();
        comboBox.getSelectionModel().selectedItemProperty().addListener(changeListener);
        addressField.textProperty().addListener(changeListener);
        minMaxPos = verticalPos;
    }

    private void initMinMax(){
        String destination = addressField.getText();
        TravelMode travelMode = comboBox.getValue();
        TravelTimeRetriever retriever;
        if(!destination.isEmpty() && travelMode != null) {
            retriever = new TravelTimeRetriever(FlatData.ADDRESS_VALUE_NAME, destination, travelMode, "Travel time transit to \"" + destination + "\".");
        }else{
            retriever = null;
        }
        initMinMax(retriever, Double.class, minMaxPos);
    }
}
