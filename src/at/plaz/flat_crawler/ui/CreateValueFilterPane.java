package at.plaz.flat_crawler.ui;

import at.plaz.flat_crawler.FlatData;
import at.plaz.flat_crawler.Value;
import at.plaz.flat_crawler.filters.*;
import at.plaz.flat_crawler.value_retriever.FlatValueRetriever;
import at.plaz.flat_crawler.value_retriever.SimpleValueRetriever;
import at.plaz.flat_crawler.value_retriever.TravelTimeRetriever;
import com.sun.javafx.collections.ObservableListWrapper;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class CreateValueFilterPane extends CreateMinMaxFilterPane {
    private int initMinMaxAt;

    public CreateValueFilterPane(Value<FlatFilter> filterValue){
        super(filterValue);
    }

    @Override
    public void initContent(GridPane pane, int verticalPos) {
        List<FlatValueRetriever<Double>> flatValueRetrievers = new LinkedList<>(FlatData.getFlatValueRetrievers(Double.class));
        ComboBox<FlatValueRetriever<Double>> comboBox = new ComboBox<>(new ObservableListWrapper<>(flatValueRetrievers));
        comboBox.setConverter(new StringConverter<FlatValueRetriever<Double>>() {
            @Override
            public String toString(FlatValueRetriever retriever) {
                return retriever.getLabel();
            }

            @Override
            public FlatValueRetriever<Double> fromString(String string) {
                return null;
            }
        });

        comboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (getLabelField().getText().isEmpty()) {
                getLabelField().setText(newValue.getLabel());
            }
            initMinMax(newValue, Double.class, initMinMaxAt);
        });
        pane.add(new Label("Filter type:"), 0, verticalPos);
        pane.add(comboBox, 1, verticalPos++);
        initMinMaxAt = verticalPos;
    }
}
