package at.plaz.flat_crawler.ui;

import at.plaz.flat_crawler.Flat;
import at.plaz.flat_crawler.Value;
import at.plaz.flat_crawler.filters.ConditionalFlatFilter;
import at.plaz.flat_crawler.filters.FlatFilter;
import at.plaz.flat_crawler.filters.IfElseFilter;
import at.plaz.flat_crawler.filters.OrFilter;

import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class CreateConditionalFilterPane extends CreateMultiFilterPane {
    public CreateConditionalFilterPane(Value<FlatFilter> flatFilter){
        super(flatFilter, "Condition", "Condition true", "Condition false");
    }

    @Override
    public FlatFilter render(List<FlatFilter> filters) {
        return new IfElseFilter(filters.get(0), filters.get(1), filters.get(2));
    }
}
