package at.plaz.flat_crawler.ui;

import at.plaz.flat_crawler.*;
import at.plaz.flat_crawler.crawler.FlatCrawler;
import at.plaz.flat_crawler.crawler.IMS24Crawler;
import at.plaz.flat_crawler.crawler.ImmoweltCrawler;
import at.plaz.flat_crawler.crawler.SalzUndBrotCrawler;
import at.plaz.flat_crawler.filters.*;
import at.plaz.flat_crawler.music.Alarm;
import at.plaz.flat_crawler.music.Jukebox;
import at.plaz.flat_crawler.music.JukeboxAlarm;
import at.plaz.flat_crawler.value_retriever.AbstractValueRetriever;
import at.plaz.flat_crawler.value_retriever.DivideValues;
import at.plaz.flat_crawler.value_retriever.FlatValueRetriever;
import com.google.maps.model.TravelMode;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.joda.time.DateTime;

import java.awt.*;
import java.util.*;
import java.util.List;


/**
 * Created by Georg Plaz.
 */
public class FilterPane extends GridPane {
    public static final String TUM_ADDRESS = "Boltzmannstraße 3, 85748 Garching bei München";
    public static final String CS_ADDRESS = "Landsbergerstraße 94, 80339 München";
    private static final Object ALARM_LOCK = new Object();
    
    public static final double DEFAULT_RENT_COLD_MIN = 400;
    public static final double DEFAULT_RENT_COLD_MAX = 1100;
    public static final double DEFAULT_RENT_TOTAL_MIN = 600;
    public static final double DEFAULT_RENT_TOTAL_MAX = 1300;
    public static final double DEFAULT_ROOMCOUNT_MIN = 1.5f;
    public static final double DEFAULT_ROOMCOUNT_MAX = 5;
    public static final int DEFAULT_TRAVEL_TIME_TUM_TRANSIT_MIN = 0;
    public static final int DEFAULT_TRAVEL_TIME_TUM_TRANSIT_MAX = 48;
    public static final int DEFAULT_TRAVEL_TIME_TUM_BICYCLE_MIN = 0;
    public static final int DEFAULT_TRAVEL_TIME_TUM_BICYCLE_MAX = 30;
    public static final int DEFAULT_TRAVEL_TIME_CS_TRANSIT_MIN = 0;
    public static final int DEFAULT_TRAVEL_TIME_CS_TRANSIT_MAX = 48;
    public static final int DEFAULT_TRAVEL_TIME_CS_BICYCLE_MIN = 0;
    public static final int DEFAULT_TRAVEL_TIME_CS_BICYCLE_MAX = 30;
    public static final int DEFAULT_SQUARE_METER_MIN = 10;
    public static final int DEFAULT_SQUARE_METER_MAX = 30;
    public static final DateTime DEFAULT_AVAILABLE_FROM_MIN = new DateTime().withYear(2016).withMonthOfYear(12).withDayOfMonth(1).minusDays(1);
    public static final DateTime DEFAULT_AVAILABLE_FROM_MAX = DEFAULT_AVAILABLE_FROM_MIN.plusMonths(2).plusDays(2);
    private final MessageDisplay messageDisplay;

    private Button startButton = new Button("Start");
    private Button stopButton = new Button("Stop");
    private Button addFilterButton = new Button("Add filter");

    private final Value<Float> refreshingIn;

    private final Label labelRefreshDelay = new Label("Refresh every n seconds:");
    private final TextField textAreaRefreshDelay = new TextField("60");
    private final TextField textAreaExecutionDelay = new TextField("0");

    private final MinMaxFilter<Double> squareMeterPriceFilter;

    private final MinMaxFilter<Double> travelTimeTUMTransitFilter;
    private final MinMaxFilter<Double> travelTimeTUMBicycleFilter;
    private final OrFilter travelTimeTUMFilter;

    private final MinMaxFilter<Double> travelTimeCSTransitFilter;
    private final MinMaxFilter<Double> travelTimeCSBicycleFilter;
    private final OrFilter travelTimeCSFilter;

    private final MinMaxFilter<Double> roomCountFilter;

    private MinMaxFilter<Double> filterRentCold;

    private MinMaxFilter<Double> filterRentTotal;

    private MinMaxFilter<DateTimeWrapper> filterAvailableFrom;

    private MenuBar menuBar;
    private Menu alertMenu;
//    private Menu alertFlatMenu;
    private RadioMenuItem noAlertMenuItem;
    private RadioMenuItem systemSoundMenuItem;
    private RadioMenuItem alarmMenuItem;
    private RadioMenuItem tuneMenuItem;

    private GridPane oldContent;
    private GridPane content;

    private Alarm alarm;

    private int verticalIndex;
    private Set<TextField> errors = new HashSet<>();
    private final List<FlatCrawler> flatCrawlers;
    private Set<FlatFilter> flatFilters = new LinkedHashSet<>();

    public FilterPane() {
        messageDisplay = new SystemOutDisplay("");

        menuBar = new MenuBar();
        alertMenu = new Menu("Alert");
//        alertFlatMenu = new Menu("Alert when new flat was found");
        noAlertMenuItem = new RadioMenuItem("No sound");
        noAlertMenuItem.selectedProperty().addListener((observable, oldValue, newValue) -> alarm = () -> {});
        systemSoundMenuItem = new RadioMenuItem("System sound");
        systemSoundMenuItem.selectedProperty().addListener((observable, oldValue, newValue) -> alarm = () -> Toolkit.getDefaultToolkit().beep());
        alarmMenuItem = new RadioMenuItem("Play (super annoying) alarm");
        alarmMenuItem.selectedProperty().addListener((observable, oldValue, newValue) -> alarm = new JukeboxAlarm(Jukebox.alarm()));
        tuneMenuItem = new RadioMenuItem("Play tune");
        tuneMenuItem.selectedProperty().addListener((observable, oldValue, newValue) -> alarm = new JukeboxAlarm(Jukebox.zelda()));

        noAlertMenuItem.selectedProperty().setValue(true);

        ToggleGroup toggleGroup = new ToggleGroup();
        noAlertMenuItem.setToggleGroup(toggleGroup);
        systemSoundMenuItem.setToggleGroup(toggleGroup);
        alarmMenuItem.setToggleGroup(toggleGroup);
        tuneMenuItem.setToggleGroup(toggleGroup);

        add(menuBar, 0, 0);

        menuBar.getMenus().add(alertMenu);
        alertMenu.getItems().addAll(noAlertMenuItem, systemSoundMenuItem, alarmMenuItem, tuneMenuItem);

        flatCrawlers = new LinkedList<>();
        flatCrawlers.add(new IMS24Crawler());
        flatCrawlers.add(new ImmoweltCrawler());
        flatCrawlers.add(new SalzUndBrotCrawler());

        GridPane buttonPane = new GridPane();
        buttonPane.add(startButton, 0, 0);
        buttonPane.add(stopButton, 1, 0);
        buttonPane.add(addFilterButton, 2, 0);
        startButton.setOnAction(event -> pressedStart());
        stopButton.setOnAction(event -> pressedStop());
        addFilterButton.setOnAction(event -> addFilterPressed());
        add(buttonPane, 0, 2);
        roomCountFilter = new DoubleMinMaxFilter(FlatData.ROOM_COUNT_VALUE_NAME, DEFAULT_ROOMCOUNT_MIN, DEFAULT_ROOMCOUNT_MAX, true);
        filterAvailableFrom = new DateFilter(FlatData.FREE_FROM_VALUE_NAME, DEFAULT_AVAILABLE_FROM_MIN, DEFAULT_AVAILABLE_FROM_MAX, false);
        filterAvailableFrom.setLabel("Filter availability date");
        filterRentCold = new DoubleMinMaxFilter(FlatData.RENT_COLD_VALUE_NAME, DEFAULT_RENT_COLD_MIN, DEFAULT_RENT_COLD_MAX, true);
        filterRentCold.setLabel("- Cold");
        filterRentTotal = new DoubleMinMaxFilter(FlatData.TOTAL_RENT_VALUE_NAME, DEFAULT_RENT_TOTAL_MIN, DEFAULT_RENT_TOTAL_MAX, true);
        filterRentTotal.setLabel("- Total");
        squareMeterPriceFilter = new DoubleMinMaxFilter(
                new DivideValues("€/m²", FlatData.RENT_COLD_VALUE_NAME, FlatData.QUADRATIC_METER_VALUE_NAME),
                DEFAULT_SQUARE_METER_MIN, DEFAULT_SQUARE_METER_MAX, true);
//        squareMeterPriceFilter = new DoubleMinMaxFilter(DEFAULT_SQUARE_METER_MIN, DEFAULT_SQUARE_METER_MAX, "By €/m²", true) {
//            @Override
//            public Double getValue(Flat flat) {
//                if(flat.getFlatData().getQuadraticMeter().has() && flat.getFlatData().getRentCold().has()){
//                    return (double)(flat.getFlatData().getRentCold().get() / flat.getFlatData().getQuadraticMeter().get());
//                }else{
//                    return null;
//                }
//            }
//        };
        ConditionalFlatFilter filterRent = new IfElseFilter(new HasValueFilter<>(FlatData.TOTAL_RENT_VALUE_NAME, Double.class), filterRentTotal, filterRentCold);
        filterRent.setLabel("Rent");

        flatFilters.add(filterRent);
        flatFilters.add(roomCountFilter);
        travelTimeTUMBicycleFilter = new TravelTimeFilter(FlatData.ADDRESS_VALUE_NAME,
                TravelMode.BICYCLING, TUM_ADDRESS, DEFAULT_TRAVEL_TIME_TUM_BICYCLE_MIN, DEFAULT_TRAVEL_TIME_TUM_BICYCLE_MAX, false);
        travelTimeTUMBicycleFilter.setLabel("- Bicycle");
        travelTimeTUMTransitFilter = new TravelTimeFilter(FlatData.ADDRESS_VALUE_NAME,
                TravelMode.TRANSIT, TUM_ADDRESS, DEFAULT_TRAVEL_TIME_TUM_TRANSIT_MIN, DEFAULT_TRAVEL_TIME_TUM_TRANSIT_MAX, false);
        travelTimeTUMTransitFilter.setLabel("- Transit");
        travelTimeTUMFilter = new OrFilter(travelTimeTUMBicycleFilter, travelTimeTUMTransitFilter);
        travelTimeTUMFilter.setLabel("Travel time TUM (in min)");
        flatFilters.add(travelTimeTUMFilter);
        travelTimeCSBicycleFilter = new TravelTimeFilter(FlatData.ADDRESS_VALUE_NAME,
                TravelMode.BICYCLING, CS_ADDRESS, DEFAULT_TRAVEL_TIME_CS_BICYCLE_MIN, DEFAULT_TRAVEL_TIME_CS_BICYCLE_MAX, false);
        travelTimeCSBicycleFilter.setLabel("- Bicycle");
        travelTimeCSTransitFilter = new TravelTimeFilter(FlatData.ADDRESS_VALUE_NAME,
                TravelMode.TRANSIT, CS_ADDRESS, DEFAULT_TRAVEL_TIME_CS_TRANSIT_MIN, DEFAULT_TRAVEL_TIME_CS_TRANSIT_MAX, false);
        travelTimeCSTransitFilter.setLabel("- Transit");
        travelTimeCSFilter = new OrFilter(travelTimeCSBicycleFilter, travelTimeCSTransitFilter);
        travelTimeCSFilter.setLabel("Travel time CS (in min)");
        flatFilters.add(travelTimeCSFilter);
        flatFilters.add(squareMeterPriceFilter);
        flatFilters.add(filterAvailableFrom);

//        for(Flat flat : flatCrawlers.iterator().next().getFlats()){
//            if (travelTimeTUMFilter.isValid(flat) && travelTimeCSFilter.isValid(flat)){
//                System.out.println(flat.getFlatData().getId());
//                System.out.println("\tTUM: "+Maps.getTravelTime(flat.getFlatData().getAddress().get(), TUM_ADDRESS, TravelMode.BICYCLING));
//                System.out.println("\tTUM: "+Maps.getTravelTime(flat.getFlatData().getAddress().get(), TUM_ADDRESS, TravelMode.TRANSIT));
//                System.out.println("\tCS:  "+Maps.getTravelTime(flat.getFlatData().getAddress().get(), CS_ADDRESS, TravelMode.BICYCLING));
//                System.out.println("\tCS:  "+Maps.getTravelTime(flat.getFlatData().getAddress().get(), CS_ADDRESS, TravelMode.TRANSIT));
//            }
//        }

        for (FlatFilter flatFilter : flatFilters){
            addFilterToCrawlers(flatFilter);
        }
        
        initContent();

        textAreaRefreshDelay.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                long sleepTime = Math.round(Float.parseFloat(textAreaRefreshDelay.getText()) * 1000);
                if(sleepTime > 0) {
                    for (FlatCrawler flatCrawler : flatCrawlers){
                        flatCrawler.getRefreshDelay().set(sleepTime);
                    }
                }
            } catch (NumberFormatException e) {
                //ignore
                messageDisplay.display("Could not parse string to refresh delay. Error: "+e.getMessage(), MessageDisplay.DEBUG_TAG);
            }
        });

        refreshingIn = new Value<>(1f);

        for(FlatCrawler flatCrawler : flatCrawlers) {
            flatCrawler.addListener(flat -> Platform.runLater(() -> {
                messageDisplay.display("Found new Flat: "+flat.getFlatData().getSite(), MessageDisplay.INFO_TAG);
                messageDisplay.display("Found new Flat: "+flat.getFlatData().getSite(), MessageDisplay.ERROR_TAG);
                new Thread(){
                    @Override
                    public void run() {
                        alarm.play();
                    }
                }.start();
                Stage flatAlert = getFlatAlert(flat);
                flatAlert.show();
            }));
        }
    }
    
    public void addContent(Node node, int x, int y){
        if(oldContent != null){
            oldContent.getChildren().remove(node);
        }
        content.add(node, x, y);
    }
    
    public void addContent(Node node, int x, int y, int width, int height){
        content.add(node, x, y, width, height);
    }

    public void initContent(){
        oldContent = content;
        getChildren().remove(oldContent);
        add(content = new GridPane(), 0, 1);
        content.setVgap(10);
        content.setHgap(10);
        content.setPadding(new Insets(10, 10, 10, 10));
        verticalIndex = 0;
        
        for(FlatFilter flatFilter : flatFilters){
            addFilterToPane(flatFilter);
        }

        addContent(new Label("Wait n minutes before execution:"), 0, verticalIndex, 2, 1);
        addContent(textAreaExecutionDelay, 2, verticalIndex++, 2, 1);

        addContent(labelRefreshDelay, 0, verticalIndex, 2, 1);
        addContent(textAreaRefreshDelay, 2, verticalIndex++, 2, 1);
    }

    public void addFilter(FlatFilter flatFilter){
        addFilterToCrawlers(flatFilter);
        addFilterToPane(flatFilter);
        flatFilters.add(flatFilter);
    }

    private void addFilterToPane(FlatFilter flatFilter){
        Button button = null;
        String primaryLabel = null;
        if(flatFilter.label() != null){
            primaryLabel = flatFilter.label();
            button = new Button("X");
            button.setOnAction(event -> {
                removeFilterFromCrawlers(flatFilter);
                flatFilters.remove(flatFilter);
                initContent();
            });
        }
        if (flatFilter instanceof MinMaxFilter) {
            MinMaxFilter<?> minMaxFilter = (MinMaxFilter) flatFilter;
                initMaxMinRow(minMaxFilter, button, primaryLabel, null);
        }else{
            for (FlatFilter subFilter : flatFilter.subFilters()){
                if (subFilter instanceof MinMaxFilter) {
                    MinMaxFilter<?> minMaxFilter = (MinMaxFilter) subFilter;
                    if(subFilter.label() != null){
                        initMaxMinRow(minMaxFilter, button, primaryLabel, subFilter.label());
                    }else{
                        initMaxMinRow(minMaxFilter, button, primaryLabel, null);
                    }
                    button = null;
                    primaryLabel = null;
                }
            }
        }
    }

    public static Stage getFlatAlert(Flat flat){
        Stage stage = new Stage();
        FlatPane flatPane = new FlatPane(flat,
                new FlatPane.AddressEntry("TUM", TUM_ADDRESS),
                new FlatPane.AddressEntry("CS", CS_ADDRESS));
        stage.setTitle("A wild flat appeared!");
        stage.setScene(new Scene(flatPane));
        stage.sizeToScene();
//        if (flat.getFlatData().getTitle().has()) {
//            dialog.setHeaderText(flat.getFlatData().getTitle().get());
//        }
//
//        dialog.getDialogPane().setContent(flatPane);
        return stage;
    }

//    private <T extends Comparable<T>> void initMaxMinRow(MinMaxFilter<T> filter, Button button) {
//        initMaxMinRow(filter, button, null, null);
//    }

    private <T extends Comparable<T>> void initMaxMinRow(MinMaxFilter<T> filter, Button button,
                                                         String primaryLabel, String secondaryLabel) {


        ValueField<T> textAreaMin = new ValueField<>(filter.getMin(), filter.stringConverter());
        ValueField<T> textAreaMax = new ValueField<>(filter.getMax(), filter.stringConverter());

        textAreaMin.getValue().addValueListener((oldValue, newValue) -> {
            if(newValue == null){
                setError(textAreaMin);
            }else{
                removeError(textAreaMin);
            }
        });
        textAreaMax.getValue().addValueListener((oldValue, newValue) -> {
            if(newValue == null){
                setError(textAreaMax);
            }else{
                removeError(textAreaMax);
            }
        });
        int primaryLabelWidth;
        if(secondaryLabel != null){
            addContent(new Label(secondaryLabel), 1, verticalIndex);
            primaryLabelWidth = 1;
        }else{
            primaryLabelWidth = 2;
        }
        if(primaryLabel != null){
            addContent(new Label(primaryLabel), 0, verticalIndex, primaryLabelWidth, 1);
        }
        if(button != null){
            addContent(button, 4, verticalIndex);
        }
        addContent(textAreaMin, 2, verticalIndex);
        addContent(textAreaMax, 3, verticalIndex++);

        textAreaMin.setPrefWidth(100);
        textAreaMax.setPrefWidth(100);
    }

    private void removeError(TextField textField){
        if(errors.remove(textField)){
            if(errors.isEmpty()){
                startButton.setDisable(false);
            }
        }
    }
    private void setError(TextField textField){
        errors.add(textField);
        startButton.setDisable(true);
    }

//    private void changeFilter(FlatFilter filter, boolean addFilter) {
//        if(addFilter){
//            addFilterToCrawlers(filter);
//        }else{
//            removeFilterFromCrawlers(filter);
//        }
//    }

    public static void main(String[] args) throws Exception {
        Thread.sleep((long)(1000 * 60 * 60 * 1.25));
        Alarm alarm = new JukeboxAlarm(Jukebox.alarm());
        alarm.play();
//        IMS24Crawler crawler = new IMS24Crawler(new SystemOutDisplay(""));
//
//        MinMaxFilter<Double> travelTimeCSTransitFilter = new TravelTimeFilter(
//                TravelMode.TRANSIT, CS_ADDRESS);
//        travelTimeCSTransitFilter.setMax(48d);
//        MinMaxFilter<Double> travelTimeCSBicycleFilter = new TravelTimeFilter(
//                TravelMode.BICYCLING, CS_ADDRESS);
//        travelTimeCSBicycleFilter.setMax(30d);
//        FlatFilter travelTimeCSFilter = new OrFilter(travelTimeCSBicycleFilter, travelTimeCSTransitFilter);
//        Collection<Flat> flats = crawler.getFlats();
//        System.out.println("found "+flats.size()+" flats");
//        Flat foundFlat = null;
//        for(Flat flat : flats){
//            if(flat.getFlatData().getId().get().equals("90329600")){
//                foundFlat = flat;
//            }
//        }
//        if(foundFlat != null){
//            System.out.println(foundFlat);
//            Maps.TravelNode node = Maps.getTravelTime(foundFlat.getFlatData().getAddress().get(), CS_ADDRESS, TravelMode.TRANSIT);
//            if(node != null){
//                System.out.println("time: "+node.getDuration().humanReadable);
//            }
//            System.out.println(travelTimeCSBicycleFilter.isValid(foundFlat));
//            System.out.println(travelTimeCSTransitFilter.isValid(foundFlat));
//            System.out.println(travelTimeCSFilter.isValid(foundFlat));
//        }
        Jukebox.playZelda();
    }

    private void addFilterToCrawlers(FlatFilter flatFilter){
        for (FlatCrawler flatCrawler : flatCrawlers) {
            flatCrawler.addFilter(flatFilter);
        }
    }
    
    private void removeFilterFromCrawlers(FlatFilter flatFilter){
        for (FlatCrawler flatCrawler : flatCrawlers) {
            flatCrawler.removeFilter(flatFilter);
        }
    }

    private void addFilterPressed(){
        FlatFilter flatFilter = showFlatFilterDialog("bla", "blu");
        if(flatFilter != null){
            addFilter(flatFilter);
        }
    }

    public static FlatFilter showFlatFilterDialog(String title, String header){
        Dialog<FlatFilter> flatFilterDialog = new Dialog<>();
        flatFilterDialog.getDialogPane().setPrefSize(400, 300);
        ButtonType okButtonType = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        flatFilterDialog.getDialogPane().getButtonTypes().addAll(okButtonType, ButtonType.CANCEL);
        CreateFilterPane createFilterPane = new CreateFilterPane();

        final Value<FlatFilter> flatFilterValue = createFilterPane.getFilterValue();
        final Node okButton = flatFilterDialog.getDialogPane().lookupButton(okButtonType);
        flatFilterValue.addValueListener((oldValue, newValue) -> okButton.setDisable(newValue == null));
        okButton.setDisable(true);

        flatFilterDialog.setTitle(title);
        flatFilterDialog.setHeaderText(header);

        flatFilterDialog.getDialogPane().setContent(createFilterPane);

        flatFilterDialog.setResultConverter(param -> param == okButtonType ? flatFilterValue.get() : null);

        Optional<FlatFilter> flatFilter = flatFilterDialog.showAndWait();
        if(flatFilter.isPresent()){
            return flatFilter.get();
        }else{
            return null;
        }
    }

    private void pressedStop(){
        for (FlatCrawler flatCrawler : flatCrawlers){
            flatCrawler.stop();
        }
    }

    private void pressedStart(){
        for(FlatCrawler flatCrawler : flatCrawlers) {
            CrawlerPane crawlerPane = new CrawlerPane(flatCrawler);
            crawlerPane.setRefreshingIn(flatCrawler.getRefreshProgress());
            flatCrawler.setMessageDisplay(crawlerPane.getDisplay());
            Stage stage = new Stage();
            stage.setTitle(flatCrawler.getSiteType()+ " - flat crawler");
            stage.setScene(new Scene(crawlerPane, 450, 700));
            stage.show();
        }
        new Thread(){
            @Override
            public void run() {
                try {
                    long sleepTime = Math.round(Float.parseFloat(textAreaExecutionDelay.getText())*60*1000);
                    if(sleepTime > 0) {
                        messageDisplay.sleepAndPrint(sleepTime);
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                for(FlatCrawler flatCrawler : flatCrawlers) {
                    flatCrawler.start();
                }
            }
        }.start();

    }
}
