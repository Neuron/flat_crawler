package at.plaz.flat_crawler.ui;

//import java.awt.*;

import javafx.scene.paint.Color;

/**
 * Created by Georg Plaz.
 */
public class Colours {
    public static String toHex(Color c){
        return String.format( "%02X%02X%02X", (int)(c.getRed()*255), (int)(c.getGreen()*255), (int)(c.getBlue()*255));
    }
    
    public static Color convert(java.awt.Color color){
        int r = color.getRed();
        int g = color.getGreen();
        int b = color.getBlue();
        int a = color.getAlpha();
        double opacity = a / 255.0 ;
        return Color.rgb(r, g, b, opacity);
    }

    public static java.awt.Color convert(Color color){
        return new java.awt.Color((float) color.getRed(),
                (float) color.getGreen(),
                (float) color.getBlue(),
                (float) color.getOpacity());
    }
}
