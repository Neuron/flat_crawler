package at.plaz.flat_crawler.ui;

import at.plaz.flat_crawler.Value;
import at.plaz.flat_crawler.ValueChangedListener;
import com.google.appengine.repackaged.com.google.common.base.Converter;
import javafx.scene.control.TextField;

/**
 * Created by Georg Plaz.
 */
public class ValueField<A> extends TextField {
    public static final String ERROR_STYLE = "-fx-text-box-border: #DBB1B1 ;\n" +
            "-fx-control-inner-background: #FFF0F0 ;";
    private final Value<A> value = new Value<>();
    private Converter<A, String> stringConverter;

    public ValueField(Converter<A, String> stringConverter){
        this.stringConverter = stringConverter;
        init();
    }

    public ValueField(String text, Converter<A, String> stringConverter) {
        this.stringConverter = stringConverter;
        init();
        setText(text);
    }

    public ValueField(A v, Converter<A, String> stringConverter) {
        super(stringConverter.convert(v));
        this.stringConverter = stringConverter;
        value.set(v);
        init();
    }

    private void init() {
        value.addValueListener((oldValue, newValue) -> setStyle(newValue != null || getText().isEmpty() ? null :
                ERROR_STYLE));
        textProperty().addListener((observable, oldValue, newValue) -> {
            try{
                value.set(stringConverter.reverse().convert(newValue));
            }catch (IllegalArgumentException e){
                value.clear();
                if(!newValue.isEmpty()) {
                    setStyle(ERROR_STYLE);
                }else{
                    setStyle(null);
                }
            }
        });
    }

    public Value<A> getValue() {
        return value;
    }
}
