package at.plaz.flat_crawler.ui;

import at.plaz.flat_crawler.Value;
import at.plaz.flat_crawler.filters.FlatFilter;
import at.plaz.flat_crawler.filters.HasValueFilter;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * Created by Georg Plaz.
 */
public class CreateFilterPane extends VBox {
    private Value<FlatFilter> filterValue = new Value<>();
    private final RadioButton simpleValue;
    private final RadioButton travelTime;
    private final RadioButton orFilter;
    private final RadioButton andFilter;
    private final RadioButton ifElseFilter;
    private final RadioButton hasValueFilter;

    public CreateFilterPane(){
        setPadding(new Insets(15, 15, 15, 15));

        GridPane filterTypePane = new GridPane();
        filterTypePane.setHgap(10);
        filterTypePane.setVgap(10);

        ToggleGroup toggleGroup = new ToggleGroup();
        int verticalIndex = 0;

        simpleValue = new RadioButton("Simple value Filter");
        simpleValue.setToggleGroup(toggleGroup);
        filterTypePane.add(simpleValue, 0, verticalIndex++);

        hasValueFilter = new RadioButton("Has Value Filter");
        hasValueFilter.setToggleGroup(toggleGroup);
        filterTypePane.add(hasValueFilter, 0, verticalIndex++);

        orFilter = new RadioButton("Logical or filter");
        orFilter.setToggleGroup(toggleGroup);
        filterTypePane.add(orFilter, 0, verticalIndex++);

        travelTime = new RadioButton("Travel time filter");
        travelTime.setToggleGroup(toggleGroup);
        filterTypePane.add(travelTime, 0, verticalIndex++);

        andFilter = new RadioButton("Logical and filter");
        andFilter.setToggleGroup(toggleGroup);
        filterTypePane.add(andFilter, 0, verticalIndex++);

        ifElseFilter = new RadioButton("If else filter");
        ifElseFilter.setToggleGroup(toggleGroup);
        filterTypePane.add(ifElseFilter, 0, verticalIndex++);

        Button nextButton = new Button("Next");
        getChildren().add(filterTypePane);
        filterTypePane.add(nextButton, 0, verticalIndex);
        toggleGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            nextButton.setDisable(newValue == null);
        });
        nextButton.setDisable(true);

        nextButton.setOnAction(event -> {
            getChildren().remove(filterTypePane);
            Node filterDetails;
            if (toggleGroup.selectedToggleProperty().getValue() == simpleValue) {
                filterDetails = new CreateValueFilterPane(filterValue);
            } else if (toggleGroup.selectedToggleProperty().getValue() == travelTime) {
                filterDetails = new CreateTravelTimeFilterPane(filterValue);
            } else if (toggleGroup.selectedToggleProperty().getValue() == orFilter) {
                filterDetails = new CreateOrFilterPane(filterValue);
            } else if (toggleGroup.selectedToggleProperty().getValue() == ifElseFilter) {
                filterDetails = new CreateConditionalFilterPane(filterValue);
            } else if (toggleGroup.selectedToggleProperty().getValue() == hasValueFilter) {
                filterDetails = new CreateHasValueFilterPane(filterValue);
            } else {
                throw new UnsupportedOperationException();
            }
            getChildren().add(filterDetails);

        });
    }

    public Value<FlatFilter> getFilterValue() {
        return filterValue;
    }
}
