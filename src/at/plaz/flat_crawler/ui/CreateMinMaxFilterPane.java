package at.plaz.flat_crawler.ui;

import at.plaz.flat_crawler.Value;
import at.plaz.flat_crawler.filters.FlatFilter;
import at.plaz.flat_crawler.filters.FlatFilterFactories;
import at.plaz.flat_crawler.filters.MinMaxFilter;
import at.plaz.flat_crawler.filters.MinMaxFilterFactory;
import at.plaz.flat_crawler.value_retriever.FlatValueRetriever;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * Created by Georg Plaz.
 */
public abstract class CreateMinMaxFilterPane extends VBox {
    private final TextField[] minField;
    private final TextField[] maxField;
    private final Label[] minLabel;
    private final Label[] maxLabel;
    private final GridPane filterDetails;
    private final TextField labelField;
    private Value<FlatFilter> filterValue;

    public CreateMinMaxFilterPane(Value<FlatFilter> filterValue) {
        setPadding(new Insets(15, 15, 15, 15));
        filterDetails = new GridPane();
        filterDetails.setHgap(10);
        filterDetails.setVgap(10);
        getChildren().add(filterDetails);
        minLabel = new Label[]{new Label("Min:")};
        maxLabel = new Label[]{new Label("Max:")};
        int verticalIndex = 0;
        this.filterValue = filterValue;
        maxField = new TextField[]{null};
        minField = new TextField[]{null};
        labelField = new TextField();

        filterDetails.add(new Label("Filter label:"), 0, verticalIndex);
        filterDetails.add(labelField, 1, verticalIndex++);

        initContent(filterDetails, verticalIndex);
    }

    public abstract void initContent(GridPane content, int verticalPos);

    public <A extends Comparable<A>> void initMinMax(FlatValueRetriever<A> retriever, Class<A> type, int verticalStartingIndex){
        String minValue;
        String maxValue;
        if(minField[0] != null){
            minValue = minField[0].getText();
            maxValue = maxField[0].getText();
            filterDetails.getChildren().removeAll(minField[0], maxField[0], minLabel[0], maxLabel[0]);
        }else{
            minValue = "";
            maxValue = "";
        }

        if(retriever != null){
            int subIndex = verticalStartingIndex;
            MinMaxFilterFactory<A> minMaxFilterFactory =
                    FlatFilterFactories.createDefaultMinMaxFilterFactory(retriever, true, type);
            MinMaxFilter<A> filter = minMaxFilterFactory.build();
            this.filterValue.set(filter);
            labelField.textProperty().addListener((observable1, oldValue1, newValue1) -> {
                filter.setLabel(newValue1);
            });
            filter.setLabel(labelField.getText());

            ValueField<A> minValueField = new ValueField<>(minValue, filter.stringConverter());
            minValueField.getValue().addValueListener((oldValue1, newValue) -> {
                if(newValue != null){
                    filter.setMin(newValue);
                }
            });
            minField[0] = minValueField;

            ValueField<A> maxValueField = new ValueField<>(maxValue, filter.stringConverter());
            maxValueField.getValue().addValueListener((oldValue1, newValue) -> {
                if(newValue != null){
                    filter.setMax(newValue);
                }
            });
            maxField[0] = maxValueField;

            filterDetails.add(minLabel[0], 0, subIndex);
            filterDetails.add(minField[0], 1, subIndex++);
            filterDetails.add(maxLabel[0], 0, subIndex);
            filterDetails.add(maxField[0], 1, subIndex++);
        }
    }
    public Value<FlatFilter> getFilterValue() {
        return filterValue;
    }

    public TextField getLabelField() {
        return labelField;
    }
}
