package at.plaz.flat_crawler;

import com.google.appengine.repackaged.org.joda.time.DateTime;

import java.time.format.DateTimeFormatter;
import java.util.Arrays;

/**
 * Created by Georg Plaz.
 */
class Message {
    private String[] tags;
    private String message;
    private String rendered;

    Message(String message, String... tags) {
        this.message = message;
        this.tags = tags;
        rendered = Arrays.toString(tags) + " - " + "[" + DateTime.now().toString("HH:mm")+ "] " + message;
    }

    public String render() {
        return rendered;
    }

    public String[] getTags() {
        return tags;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "tags=" + Arrays.toString(tags) +
                ", message='" + message + '\'' +
                '}';
    }
}
