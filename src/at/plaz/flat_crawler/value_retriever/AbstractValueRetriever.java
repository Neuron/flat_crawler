package at.plaz.flat_crawler.value_retriever;

import at.plaz.flat_crawler.Flat;
import at.plaz.flat_crawler.Value;

import java.lang.reflect.Method;

/**
 * Created by Georg Plaz.
 */
public abstract class AbstractValueRetriever<A> implements FlatValueRetriever<A> {
    private String label;
    private Class<A> type;

    public AbstractValueRetriever(String label, Class<A> type) {
        this.label = label;
        this.type = type;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public Class<?> getType() {
        return type;
    }
}
