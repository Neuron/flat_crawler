package at.plaz.flat_crawler.value_retriever;

import at.plaz.flat_crawler.*;
import com.google.maps.model.Duration;
import com.google.maps.model.TravelMode;

/**
 * Created by Georg Plaz.
 */
public class TravelTimeRetriever extends AbstractValueRetriever<Double> {
    private FlatValueRetriever<String> addressRetriever;
    private final String travelToAddress;
    private final TravelMode travelMode;

    public TravelTimeRetriever(String valueName, String travelToAddress, TravelMode travelMode, String label) {
        super(label, Double.class);
        addressRetriever = FlatData.getRetriever(valueName, String.class);
        this.travelToAddress = travelToAddress;
        this.travelMode = travelMode;
    }

    @Override
    public Value<Double> retrieve(Flat flat) {
        Value<String> address = addressRetriever.retrieve(flat);
        Value<Double> travelTime = new Value<>(toTravelTime(address));
        address.addValueListener((oldValue, newValue) -> travelTime.set(toTravelTime(address)));
        return travelTime;
    }

    @Override
    public Class<?> getType() {
        return Double.class;
    }

    private Double toTravelTime(Value<String> address){
        if(address.has()) {
            Maps.TravelNode travelNode = Maps.getTravelTime(address.get(), travelToAddress, travelMode);
            if (travelNode != null) {
                Duration duration = travelNode.getDuration();
                if (duration == null) {
                    return null;
                } else {
                    return (double) duration.inSeconds / 60.;
                }
            } else {
                return null;
            }
        }else{
            return null;
        }
    }
}
