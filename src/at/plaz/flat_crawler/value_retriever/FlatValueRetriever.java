package at.plaz.flat_crawler.value_retriever;

import at.plaz.flat_crawler.Flat;
import at.plaz.flat_crawler.Value;

/**
 * Created by Georg Plaz.
 */
public interface FlatValueRetriever<A> {
    Value<A> retrieve(Flat flat);

    String getLabel();

    Class<?> getType();
}
