package at.plaz.flat_crawler.value_retriever;

import at.plaz.flat_crawler.Flat;
import at.plaz.flat_crawler.Type;
import at.plaz.flat_crawler.Value;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Georg Plaz.
 */
public class SimpleValueRetriever<A> extends AbstractValueRetriever<A> {
    private Method method;
    private String valueName;

    public SimpleValueRetriever(Method flatDataMethod, String label, Class<A> type) {
        super(label, type);
        this.method = flatDataMethod;
        Type methodType = method.getDeclaredAnnotation(Type.class);
        valueName = methodType.name();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Value<A> retrieve(Flat flat) {
        try {
            return (Value<A>) method.invoke(flat.getFlatData());
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return new Value<>();
        }
    }

    @Override
    public String toString() {
        return "SimpleValueRetriever{" +
                "value name=\"" + valueName + "\", " +
                "type=" + getType() +
                '}';
    }

    public String getValueName() {
        return valueName;
    }
}
