package at.plaz.flat_crawler.value_retriever;

import at.plaz.flat_crawler.Flat;
import at.plaz.flat_crawler.FlatData;
import at.plaz.flat_crawler.Value;
import at.plaz.flat_crawler.ValueChangedListener;

/**
 * Created by Georg Plaz.
 */
public class DivideValues extends AbstractValueRetriever<Double> {
    private FlatValueRetriever<Double> first;
    private FlatValueRetriever<Double> second;

    public DivideValues(String label, String firstValueName, String secondValueName) {
        super(label, Double.class);
        first = FlatData.getRetriever(firstValueName, Double.class);
        second = FlatData.getRetriever(secondValueName, Double.class);
    }

    public DivideValues(String label, FlatValueRetriever<Double> first, FlatValueRetriever<Double> second) {
        super(label, Double.class);
        this.first = first;
        this.second = second;
    }

    @Override
    public Value<Double> retrieve(Flat flat) {
        Value<Double> firstValue = first.retrieve(flat);
        Value<Double> secondValue = second.retrieve(flat);
        Value<Double> result = new Value<>();
        ValueChangedListener<Double> listener = (oldValue, newValue) -> {
            result.set(firstValue.has() && secondValue.has() ? firstValue.get() / secondValue.get() : null);
        };
        firstValue.addValueListener(listener);
        secondValue.addValueListener(listener);
        return result;
    }

    @Override
    public String toString() {
        return "DivideValues{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }
}
