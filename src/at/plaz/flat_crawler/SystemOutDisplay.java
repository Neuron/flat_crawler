package at.plaz.flat_crawler;

import java.util.*;

/**
 * Created by Georg Plaz.
 */
public class SystemOutDisplay implements MessageDisplay {
    private String prefix;

    public SystemOutDisplay(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public void display(Message message) {
        String messageText = prefix+": "+message.render();
        for (String tag : message.getTags()){
            if(tag.equals(ERROR_TAG)){
                System.err.println(messageText);
                return;
            }
        }
        System.out.println(messageText);
    }
}
