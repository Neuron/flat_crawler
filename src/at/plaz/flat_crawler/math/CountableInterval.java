package at.plaz.flat_crawler.math;

import java.util.Set;

/**
 * Created by Georg Plaz.
 */
public abstract class CountableInterval<A extends Comparable<A>> extends Interval<A> {
    /**
     * @see #Interval(Comparable, Comparable)
     */
    public CountableInterval(A lower, A upper) {
        super(lower, upper);
    }

    /**
     * @see #Interval(Comparable)
     */
    public CountableInterval(A lower) {
        super(lower);
    }

    /**
     * @see #Interval(Interval)
     */
    public CountableInterval(Interval<A> toCopy) {
        super(toCopy);
    }

    /**
     * @see #Interval(Comparable, Interval.InclusionType, Comparable, Interval.InclusionType)
     */
    public CountableInterval(A lower, Interval.InclusionType lowerInclusion, A upper, Interval.InclusionType upperInclusion) {
        super(lower, lowerInclusion, upper, upperInclusion);
    }

    /**
     * Converts all values which lie within this interval into a set.<p>
     * The set for the integer interval [1, 5) will return the elements {1, 2, 3, 4}.
     * @return The set containing all values which lie in this interval.
     */
    public abstract Set<A> toSet();

    /**
     * This method will check how many distinct values lie within the specified interval. For example
     * <code>size()</code> for the integer interval [1, 5) will return 4, since it contains the elements {1, 2, 3, 4}.
     * <p>
     * If <code>isFinite()</code> is true, <code>size()</code> is also equivalent to <code>toSet().size()</code>
     * @return The number of distinct objects in this interval, or -1 if this interval has an infinite number of values
     */
    public int size(){
        if(isFinite()){
            return toSet().size();
        }else{
            return -1;
        }
    }

    /**
     * Checks if this interval has a finite amount of values. Since this class operates on values from a countable set,
     * this call is equivalent to <code>hasUpperBound() && hasLowerBound()</code>
     * @return If the interval has a finite amount of values.
     */
    public boolean isFinite(){
        return hasUpperBound() && hasLowerBound();
    }

    /**
     * Normalizes this interval from any form of inclusive and exclusive boundaries to the form [lower, upper). This is
     * achieved by changing the lower and upper boundaries, to create a mathematically equivalent set. Please note,
     * that the normalized interval does not have to be equal to the initial interval. This is the case, because
     * converting an {@link IntegerInterval} to a {@link DoubleInterval}.
     * @return
     */
    public abstract CountableInterval<A> normalize();

}
