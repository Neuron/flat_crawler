package at.plaz.flat_crawler.math;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Objects;

/**
 * Created by Georg Plaz.
 *
 * This class defines an interval. The bounds can be inclusive and exclusive.
 */
public class Interval<A extends Comparable<A>>{ //todo check comments for occurrences of integer or A
    private final A lower;
    private final InclusionType lowerInclusion;
    private final A upper;
    private final InclusionType upperInclusion;

    /**
     * Creates an interval, defined as [lower, upper), which means that the lower bound is inclusive and the upper
     * bound is exclusive.
     * If any of the values is null, that bound is not considered. IntervalValidator(lower, null) will perform a
     * check as [lower, infinity) and IntervalValidator(null, upper) checks for [-infinity, upper).
     * @param lower the lower bound (inclusive)
     * @param upper the upper bound (exclusive)
     */
    public Interval(A lower, A upper) {
        this(lower, InclusionType.INCLUSIVE, upper, InclusionType.EXCLUSIVE);
    }

    /**
     * Creates an interval defined as [lower, inf)
     * @param lower the lower bound
     */
    public Interval(A lower) {
        this(lower, InclusionType.INCLUSIVE, null, InclusionType.EXCLUSIVE);
    }

    /**
     * This simple copy constructor copies an interval. Calling <code>equals(toCopy)</code> will always return <code>true</code>.
     * @param toCopy the interval which should be copied.
     */
    public Interval(Interval<A> toCopy) {
        this(toCopy.getLowerBound(), toCopy.getLowerInclusion(), toCopy.getUpperBound(), toCopy.getUpperInclusion());
    }

    /**
     * Creates an interval, where the bounds can be inclusive or exclusive.
     * If any of the values is null, that bound is not considered. IntervalValidator(lower, INCLUSIVE, null, INCLUSIVE)
     * will perform a check as [lower, infinity].
     * @param lower the lower bound
     * @param lowerInclusion if the lower bound is inclusive or exclusive.
     *                       Can be InclusionType.INCLUSIVE or InclusionType.EXCLUSIVE
     * @param upper the upper bound
     * @param upperInclusion if the upper bound is inclusive or exclusive.
     *                       Can be InclusionType.INCLUSIVE or InclusionType.EXCLUSIVE
     */
    public Interval(A lower, InclusionType lowerInclusion, A upper, InclusionType upperInclusion) {
        this.lower = lower;
        this.lowerInclusion = lowerInclusion;
        this.upper = upper;
        this.upperInclusion = upperInclusion;
    }

    /**
     * Checks if a number lies within the interval
     * @param toCheck the number to check
     * @return if the number lies within the interval
     */

    public boolean contains(A toCheck){ //todo create non generic implementations for extending classes
        boolean lowerCondition = lower==null
                || (lowerInclusion == InclusionType.INCLUSIVE ? lower.compareTo(toCheck) <= 0 : lower.compareTo(toCheck) < 0);
        boolean upperCondition = upper==null
                || (upperInclusion == InclusionType.INCLUSIVE ? toCheck.compareTo(upper) <= 0: toCheck.compareTo(upper) < 0); //todo refactor into return call
        return lowerCondition && upperCondition;

    }

    public Interval<A> intersect(Interval<A> other) { //todo return this or other in certain cases..
        A highestLower;
        InclusionType lowerInclusion;
        if(!hasLowerBound() || (other.hasLowerBound() && other.getLowerBound().compareTo(getLowerBound())>0)){
            highestLower = other.getLowerBound();
            lowerInclusion = other.getLowerInclusion();
        }else if(!other.hasLowerBound() || (other.hasLowerBound() && getLowerBound().compareTo(other.getLowerBound())>0)){
            highestLower = getLowerBound();
            lowerInclusion = getLowerInclusion();
        }else if(Objects.equals(getLowerBound(), other.getLowerBound())){
            highestLower = getLowerBound();
            lowerInclusion = InclusionType.values()[Math.max(getLowerInclusion().ordinal(), other.getLowerInclusion().ordinal())];
        }else{
            throw new RuntimeException("wtf?! this: "+this+", other: "+other);
        }

        A lowestUpper;
        InclusionType upperInclusion;
        if(!hasUpperBound() || (other.hasUpperBound() && other.getUpperBound().compareTo(getUpperBound())<0)){
            lowestUpper = other.getUpperBound();
            upperInclusion = other.getUpperInclusion();
        }else if(!other.hasUpperBound() || (other.hasUpperBound() && getUpperBound().compareTo(other.getUpperBound())<0)){
            lowestUpper = getUpperBound();
            upperInclusion = getUpperInclusion();
        }else if(Objects.equals(getUpperBound(), other.getUpperBound())){
            lowestUpper = getUpperBound();
            upperInclusion = InclusionType.values()[Math.max(getUpperInclusion().ordinal(), other.getUpperInclusion().ordinal())];
        }else{
            throw new RuntimeException("wtf?! this: "+this+", other: "+other);
        }

        return new Interval<>(highestLower, lowerInclusion, lowestUpper, upperInclusion);
    }

    //public abstract AbstractInterval<A> toInterval(A lower, InclusionType lowerInclusion, A upper, InclusionType upperInclusion);

    public enum InclusionType{
        INCLUSIVE(new char[]{'[',']'}), EXCLUSIVE(new char[]{'(',')'});
        private final char[] chars;

        InclusionType(char[] chars) {
            this.chars = chars;
        }

        public static InclusionType parse(char c){
            switch (c){
                case '(':
                case ')':
                    return EXCLUSIVE;
                case '[':
                case ']':
                    return INCLUSIVE;
                default:
                    return null;
            }
        }

        public char toChar(boolean opening) {
            return chars[opening?0:1];
        }
    }

    public A getLowerBound() {
        return lower;
    }

    public A getUpperBound() {
        return upper;
    }

    public boolean isLowerInclusive() {
        return lowerInclusion== InclusionType.INCLUSIVE;
    }

    public boolean isUpperInclusive() {
        return upperInclusion== InclusionType.INCLUSIVE;
    }

    public InclusionType getUpperInclusion() {
        return upperInclusion;
    }

    public InclusionType getLowerInclusion() {
        return lowerInclusion;
    }

    public boolean hasLowerBound(){
        return lower!=null;
    }

    public boolean hasUpperBound(){
        return upper!=null;
    }




    @Override
    public String toString() {
        return lowerInclusion.toChar(true)+
                (hasLowerBound()?""+getLowerBound():"-inf")+","+
                (hasUpperBound()?""+getUpperBound():"inf")+
                upperInclusion.toChar(false);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Interval)) return false;

        Interval interval = (Interval) o;

        if (lower != null ? !lower.equals(interval.lower) : interval.lower != null) return false;
        if (getLowerInclusion() != interval.getLowerInclusion()) return false;
        if (upper != null ? !upper.equals(interval.upper) : interval.upper != null) return false;
        return getUpperInclusion() == interval.getUpperInclusion();

    }

    @Override
    public int hashCode() {
        int result = lower != null ? lower.hashCode() : 0;
        result = 31 * result + (getLowerInclusion() != null ? getLowerInclusion().hashCode() : 0);
        result = 31 * result + (upper != null ? upper.hashCode() : 0);
        result = 31 * result + (getUpperInclusion() != null ? getUpperInclusion().hashCode() : 0);
        return result;
    }

    /**
     * Creates an interval from a user input. The input has to have the standard interval syntax, with (lower, upper)
     * defining an exclusive and [lower, upper] defining an inclusive interval.
     * @param boundsToParse
     * @throws InvalidInputException If the input had an invalid syntax
     */
    public static Pair<Pair<String, String>, Pair<InclusionType, InclusionType>> parse(String boundsToParse) throws InvalidInputException{
        String lower;
        InclusionType lowerInclusion;
        String upper;
        InclusionType upperInclusion;
        boundsToParse = boundsToParse.trim();
        lowerInclusion = InclusionType.parse(boundsToParse.charAt(0));
        if(lowerInclusion==null){
            throw new InvalidInputException(boundsToParse, "the first character of a interval input must either be \'(\' (exclusive)" +
                    "ot \'[\' (inclusive). Input was \""+boundsToParse+"\"");
        }
        upperInclusion = InclusionType.parse(boundsToParse.charAt(boundsToParse.length() - 1));
        if(upperInclusion==null){
            throw new InvalidInputException(boundsToParse, "last character of a interval input must either be \')\' (exclusive)" +
                    "ot \']\' (inclusive). Input was \""+boundsToParse+"\"");
        }
        int commaPos = boundsToParse.indexOf(',');
        if(commaPos==-1){
            throw new InvalidInputException(boundsToParse, "the input \""+boundsToParse+"\" did not contain a comma!");
        }
        String lowerString = boundsToParse.substring(1, commaPos).trim();
        if (lowerString.equalsIgnoreCase("inf")) {
            throw new InvalidInputException(boundsToParse, "lower bound must not be \"inf\". did you mean \"-inf?\"");
        }
        boolean lowerIsNegativeInf = lowerString.equalsIgnoreCase("null") || lowerString.equalsIgnoreCase("-inf");
        try {
            lower = lowerIsNegativeInf ? null : lowerString;
        }catch (NumberFormatException e){
            throw new InvalidInputException(boundsToParse, "couldn't parse \""+lowerString+"\" to a number");
        }
        String upperString = boundsToParse.substring(commaPos + 1, boundsToParse.length() - 1).trim();
        if (upperString.equalsIgnoreCase("-inf")) {
            throw new InvalidInputException(boundsToParse, "upper bound must not be \"-inf\". did you mean \"inf?\"");
        }
        boolean upperIsNegativeInf = upperString.equalsIgnoreCase("null") || upperString.equalsIgnoreCase("inf");
        try {
            upper = upperIsNegativeInf ? null : upperString;
        }catch (NumberFormatException e){
            throw new InvalidInputException(boundsToParse, "couldn't parse \""+upperString+"\" to a number", e);
        }
        return new ImmutablePair<>(new ImmutablePair<>(lower, upper), new ImmutablePair<>(lowerInclusion, upperInclusion));
        /*if(hasLowerBound()&&hasUpperBound()&&lower>upper){
            throw new InvalidInputException(boundsToParse, "lower bound must not be greater than upper bound!" +
                    " Input was: "+boundsToParse);
        }*/
    }
}
