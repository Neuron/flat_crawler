package at.plaz.flat_crawler.math;

import org.apache.commons.lang3.tuple.Pair;

/**
 * Created by Georg Plaz.<p>
 * This class defines an interval for Doubles. The bounds can be inclusive and exclusive.
 */
public class DoubleInterval extends Interval<Double> {

    /**
     * @see #Interval(Comparable, Comparable)
     */
    public DoubleInterval(Double lower, Double upper) {
        super(lower, upper);
    }

    /**
     * @see #Interval(Comparable, Comparable)
     */
    public DoubleInterval(Double lower, double upper) {
        super(lower, upper);
    }

    /**
     * @see #Interval(Comparable, Comparable)
     */
    public DoubleInterval(double lower, Double upper) {
        super(lower, upper);
    }

    /**+
     * @see #Interval(Comparable, Comparable)
     */
    public DoubleInterval(double lower, double upper) {
        super(lower, upper);
    }

    /**
     * @see #Interval(Comparable)
     */
    public DoubleInterval(Double lower) {
        super(lower);
    }

    /**
     * @see #Interval(Comparable)
     */
    public DoubleInterval(double lower) {
        super(lower);
    }

    /**
     * @see #Interval(Interval)
     */
    public DoubleInterval(Interval<Double> interval) {
        super(interval);
    }

    /**
     * @see #Interval(Comparable, InclusionType, Comparable, InclusionType)
     */
    public DoubleInterval(Double lower, InclusionType lowerInclusion, Double upper, InclusionType upperInclusion) {
        super(lower, lowerInclusion, upper, upperInclusion);
    }

    /**
     * @see #Interval(Comparable, InclusionType, Comparable, InclusionType)
     */
    public DoubleInterval(double lower, InclusionType lowerInclusion, double upper, InclusionType upperInclusion) {
        super(lower, lowerInclusion, upper, upperInclusion);
    }

    private DoubleInterval(Pair<Pair<String, String>, Pair<InclusionType, InclusionType>> values) throws InvalidInputException {
        super(values.getLeft().getLeft()==null?null:parseDouble(values.getLeft().getLeft()), values.getRight().getLeft(),
                values.getLeft().getRight()==null?null:parseDouble(values.getLeft().getRight()), values.getRight().getRight());
    }


    /**
     * Creates an interval from a user input. The input has to have the standard interval syntax, with (lower, upper)
     * defining an exclusive and [lower, upper] defining an inclusive interval.
     * @param boundsToParse the interval which will be parsed
     */
    public DoubleInterval(String boundsToParse) throws InvalidInputException{
        this(parse(boundsToParse));
    }

    @Override
    public DoubleInterval intersect(Interval<Double> other) { //todo more efficient implementation
        return new DoubleInterval(super.intersect(other));
    }

//    public DoubleInterval intersect(DoubleInterval other) {
//        Double highestLower;
//        InclusionType lowerInclusion;
//        if(!hasLowerBound() || (other.hasLowerBound() && other.getLowerBound()>getLowerBound())){
//            highestLower = other.getLowerBound();
//            lowerInclusion = other.getLowerInclusion();
//        }else if(!other.hasLowerBound() || (other.hasLowerBound() && getLowerBound()>other.getLowerBound())){
//            highestLower = getLowerBound();
//            lowerInclusion = getLowerInclusion();
//        }else if(Objects.equals(getLowerBound(), other.getLowerBound())){
//            highestLower = getLowerBound();
//            lowerInclusion = InclusionType.values()[Math.max(getLowerInclusion().ordinal(), other.getLowerInclusion().ordinal())];
//        }else{
//            throw new RuntimeException("wtf?! this: "+this+", other: "+other);
//        }
//
//        Double lowestUpper;
//        InclusionType upperInclusion;
//        if(!hasUpperBound() || (other.hasUpperBound() && other.getUpperBound()<getUpperBound())){
//            lowestUpper = other.getUpperBound();
//            upperInclusion = other.getUpperInclusion();
//        }else if(!other.hasUpperBound() || (other.hasUpperBound() && getUpperBound()<other.getUpperBound())){
//            lowestUpper = getUpperBound();
//            upperInclusion = getUpperInclusion();
//        }else if(Objects.equals(getUpperBound(), other.getUpperBound())){
//            lowestUpper = getUpperBound();
//            upperInclusion = InclusionType.values()[Math.max(getUpperInclusion().ordinal(), other.getUpperInclusion().ordinal())];
//        }else{
//            throw new RuntimeException("wtf?! this: "+this+", other: "+other);
//        }
//
//        return new DoubleInterval(highestLower, lowerInclusion, lowestUpper, upperInclusion);
//    }

    public boolean contains(double toCheck) {
        return super.contains(toCheck);
    }


    private static Double parseDouble(String input) throws InvalidInputException {
        try{
            return new Double(input);
        }catch (NumberFormatException e){
            throw new InvalidInputException(input, "The input has to be a valid floating point number, but it was \""+input+"\"", e);
        }
    }
}
