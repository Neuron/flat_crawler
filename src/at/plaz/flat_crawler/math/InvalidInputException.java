package at.plaz.flat_crawler.math;

/**
 * Created by Georg Plaz.
 */
public class InvalidInputException extends Exception {
    private Object object;

    public InvalidInputException(Object object, String s, Exception e) {
        super(s, e);
        this.object = object;
    }

    public InvalidInputException(Object object, String s) {
        super(s);
        this.object = object;
    }

    public Object getObject() {
        return object;
    }
}
