package at.plaz.flat_crawler.math;

import org.apache.commons.lang3.tuple.Pair;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Georg Plaz.<p>
 * This class defines an interval for integers. The bounds can be inclusive and exclusive.
 */
public class IntegerInterval extends CountableInterval<Integer> {

    /**
     * @see #Interval(Comparable, Comparable)
     */
    public IntegerInterval(Integer lower, Integer upper) {
        super(lower, upper);
    }

    /**
     * @see #Interval(Comparable)
     */
    public IntegerInterval(Integer lower) {
        super(lower);
    }

    /**
     * @see #Interval(Interval)
     */
    public IntegerInterval(Interval<Integer> interval) {
        super(interval);
    }

    /**
     * @see #Interval(Comparable, Interval.InclusionType, Comparable, Interval.InclusionType)
     */
    public IntegerInterval(Integer lower, Interval.InclusionType lowerInclusion, Integer upper, Interval.InclusionType upperInclusion) {
        super(lower, lowerInclusion, upper, upperInclusion);
    }

    private IntegerInterval(Pair<Pair<String, String>, Pair<Interval.InclusionType, Interval.InclusionType>> values) throws InvalidInputException {
        super(values.getLeft().getLeft()==null?null:parseInt(values.getLeft().getLeft()), values.getRight().getLeft(),
                values.getLeft().getRight()==null?null:parseInt(values.getLeft().getRight()), values.getRight().getRight());
    }


    /**
     * Creates an interval from a user input. The input has to have the standard interval syntax, with (lower, upper)
     * defining an exclusive and [lower, upper] defining an inclusive interval.
     * @param boundsToParse the interval which will be parsed
     */
    public IntegerInterval(String boundsToParse) throws InvalidInputException{
        this(parse(boundsToParse));
    }

//    public IntegerInterval intersect(IntegerInterval other) {
//        Integer highestLower;
//        InclusionType lowerInclusion;
//        if(!hasLowerBound() || (other.hasLowerBound() && other.getLowerBound()>getLowerBound())){
//            highestLower = other.getLowerBound();
//            lowerInclusion = other.getLowerInclusion();
//        }else if(!other.hasLowerBound() || (other.hasLowerBound() && getLowerBound()>other.getLowerBound())){
//            highestLower = getLowerBound();
//            lowerInclusion = getLowerInclusion();
//        }else if(Objects.equals(getLowerBound(), other.getLowerBound())){
//            highestLower = getLowerBound();
//            lowerInclusion = InclusionType.values()[Math.max(getLowerInclusion().ordinal(), other.getLowerInclusion().ordinal())];
//        }else{
//            throw new RuntimeException("wtf?! this: "+this+", other: "+other);
//        }
//
//        Integer lowestUpper;
//        InclusionType upperInclusion;
//        if(!hasUpperBound() || (other.hasUpperBound() && other.getUpperBound()<getUpperBound())){
//            lowestUpper = other.getUpperBound();
//            upperInclusion = other.getUpperInclusion();
//        }else if(!other.hasUpperBound() || (other.hasUpperBound() && getUpperBound()<other.getUpperBound())){
//            lowestUpper = getUpperBound();
//            upperInclusion = getUpperInclusion();
//        }else if(Objects.equals(getUpperBound(), other.getUpperBound())){
//            lowestUpper = getUpperBound();
//            upperInclusion = InclusionType.values()[Math.max(getUpperInclusion().ordinal(), other.getUpperInclusion().ordinal())];
//        }else{
//            throw new RuntimeException("wtf?! this: "+this+", other: "+other);
//        }
//
//        return new IntegerInterval(highestLower, lowerInclusion, lowestUpper, upperInclusion);
//    }

    @Override
    public int size(){
        if(!hasLowerBound() || !hasUpperBound()){
            return -1;
        }else{
            IntegerInterval normalized = normalize();
            return normalized.getUpperBound()-normalized.getLowerBound();
        }
    }

    public DoubleInterval toDoubleInterval(){ //todo create test cases
        return new DoubleInterval(hasLowerBound()?getLowerBound().doubleValue():null, getLowerInclusion(),
                                  hasUpperBound()?getUpperBound().doubleValue():null, getUpperInclusion());
    }

    @Override
    public IntegerInterval normalize(){
        if(isLowerInclusive() && !isUpperInclusive()){
            return this;
        }
        Integer lower = getLowerBound();
        if(hasLowerBound() && !isLowerInclusive()){
            lower+=1;
        }
        Integer upper = getUpperBound();
        if(hasUpperBound() && isUpperInclusive()){
            upper+=1;
        }
        return new IntegerInterval(lower, InclusionType.INCLUSIVE, upper, InclusionType.EXCLUSIVE);
    }

    @Override
    public Set<Integer> toSet(){
        if(!hasUpperBound() || !hasLowerBound()){
            return null;
        }else{
            IntegerInterval normalized = normalize();
            Set<Integer> toReturn = new LinkedHashSet<>(normalized.size()); //todo create test cases for insertion order
            for(int i=normalized.getLowerBound(); i<normalized.getUpperBound(); i++){
                toReturn.add(i);
            }
            return toReturn;
        }
    }

    @Override
    public IntegerInterval intersect(Interval<Integer> other) { //todo more efficient implementation
        return new IntegerInterval(super.intersect(other));
    }

    private static Integer parseInt(String input) throws InvalidInputException {
        try{
            return new Integer(input);
        }catch (NumberFormatException e){
            throw new InvalidInputException(input, "The input has to be a valid whole number, but it was \""+input+"\"", e);
        }
    }
}
