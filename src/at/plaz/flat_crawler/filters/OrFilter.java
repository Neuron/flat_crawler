package at.plaz.flat_crawler.filters;

import at.plaz.flat_crawler.Flat;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class OrFilter extends AbstractFlatFilter {
    private FlatFilter first;
    private FlatFilter second;
    private List<FlatFilter> flatFilters;

    public OrFilter(FlatFilter first, FlatFilter second) {
        this.first = first;
        this.second = second;
        flatFilters = Arrays.asList(first, second);
    }

    @Override
    public boolean isValid(Flat flat) {
        return first.isValid(flat) || second.isValid(flat);
    }

    @Override
    public String toString() {
        return "OrFilter{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }

    @Override
    public List<FlatFilter> subFilters() {
        return flatFilters;
    }

    @Override
    public String toReadableString() {
        return "either: ("+first+") or: ("+second+").";
    }
}
