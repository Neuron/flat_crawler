package at.plaz.flat_crawler.filters;

import at.plaz.flat_crawler.value_retriever.FlatValueRetriever;
import at.plaz.flat_crawler.value_retriever.SimpleValueRetriever;
import com.google.appengine.repackaged.com.google.common.base.Converter;

/**
 * Created by Georg Plaz.
 */
public class StandardMinMaxFilterFactory<A extends Comparable<A>> implements MinMaxFilterFactory<A>{
    private FlatValueRetriever<A> valueRetriever;
    private boolean validityIfMissing;
    private Converter<A, String> converter;

    public StandardMinMaxFilterFactory(FlatValueRetriever<A> valueRetriever, boolean validityIfMissing, Converter<A, String> converter) {
        this.valueRetriever = valueRetriever;
        this.validityIfMissing = validityIfMissing;
        this.converter = converter;
    }

    @Override
    public MinMaxFilter<A> build() {
        return new SimpleFlatFilter<A>(valueRetriever, validityIfMissing, converter);
    }
}
