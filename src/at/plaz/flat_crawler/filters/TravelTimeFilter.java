package at.plaz.flat_crawler.filters;

import at.plaz.flat_crawler.Flat;
import at.plaz.flat_crawler.Maps;
import at.plaz.flat_crawler.Value;
import at.plaz.flat_crawler.value_retriever.FlatValueRetriever;
import at.plaz.flat_crawler.value_retriever.SimpleValueRetriever;
import at.plaz.flat_crawler.value_retriever.TravelTimeRetriever;
import com.google.maps.model.Duration;
import com.google.maps.model.TravelMode;

/**
 * Created by Georg Plaz.
 */
public class TravelTimeFilter extends DoubleMinMaxFilter {
    private TravelMode travelMode;
    private String addressTo;

    public TravelTimeFilter(FlatValueRetriever<Double> retriever, TravelMode travelMode, String addressTo, boolean validityIfMissingDate) {
        this(retriever, travelMode, addressTo, 0, 0, validityIfMissingDate);
    }

    public TravelTimeFilter(String valueName, TravelMode travelMode, String addressTo, boolean validityIfMissingDate) {
        this(valueName, travelMode, addressTo, 0, 0, validityIfMissingDate);
    }

//    public TravelTimeFilter(TravelMode travelMode, String addressTo, float min, float max, boolean validityIfMissingDate) {
//        this(travelMode, addressTo, min, max, validityIfMissingDate);
//    }

    public TravelTimeFilter(FlatValueRetriever<Double> retriever, TravelMode travelMode, String addressTo, float min, float max, boolean validityIfMissingDate) {
        super(retriever, (double)min, (double)max, validityIfMissingDate);
        this.travelMode = travelMode;
        this.addressTo = addressTo;
    }

    public TravelTimeFilter(String valueName, TravelMode travelMode, String addressTo, float min, float max, boolean validityIfMissingDate) {
        super(new TravelTimeRetriever(valueName, addressTo, travelMode, "label bla"), (double)min, (double)max, validityIfMissingDate);
        this.travelMode = travelMode;
        this.addressTo = addressTo;
    }

//    @Override
//    public Double getValue(Flat flat) {
//        Value<String> address = flat.getFlatData().getAddress();
//        if(!address.has()){
//            return null;
//        }
//        Maps.TravelNode travelNode = Maps.getTravelTime(flat.getFlatData().getAddress().get(), addressTo, travelMode);
//        if(travelNode != null) {
//            Duration duration = travelNode.getDuration();
//            if(duration == null){
//                return null;
//            }else{
//                return (double) duration.inSeconds/60.;
//            }
//        } else {
//            return null;
//        }
//    }

    @Override
    public String toString() {
        return "TravelTimeFilter{" +
                "travelMode=" + travelMode +
                ", addressTo='" + addressTo + "\'" +
                ", min='" + getMin() + "\'" +
                ", max='" + getMax() + '\'' +
                '}';
    }
}
