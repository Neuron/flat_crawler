package at.plaz.flat_crawler.filters;

import at.plaz.flat_crawler.value_retriever.FlatValueRetriever;
import com.google.appengine.repackaged.com.google.common.base.Converter;

/**
 * Created by Georg Plaz.
 */
public class DoubleMinMaxFilter extends SimpleFlatFilter<Double> {
    private static final Converter<Double, String> converter = new Converter<Double, String>() {
        @Override
        protected String doForward(Double aDouble) {
            return aDouble.toString();
        }

        @Override
        protected Double doBackward(String s) {
            return Double.valueOf(s);
        }
    };

    public static Converter<Double, String> getConverter() {
        return converter;
    }

    public DoubleMinMaxFilter(String valueName, boolean validityIfMissing) {
        super(valueName, validityIfMissing, Double.class, converter);
    }

    public DoubleMinMaxFilter(String valueName, double min, double max, boolean validityIfMissing) {
        super(valueName, min, max, validityIfMissing, Double.class, converter);
    }

    public DoubleMinMaxFilter(FlatValueRetriever<Double> retriever, boolean validityIfMissing) {
        super(retriever, validityIfMissing, converter);
    }

    public DoubleMinMaxFilter(FlatValueRetriever<Double> retriever, Double min, Double max, boolean validityIfMissing) {
        super(retriever, min, max, validityIfMissing, converter);
    }

    public DoubleMinMaxFilter(FlatValueRetriever<Double> retriever, double min, double max, boolean validityIfMissing) {
        super(retriever, min, max, validityIfMissing, converter);
    }

    public void setMin(double min) {
        super.setMin(min);
    }

    public void setMax(double min) {
        super.setMax(min);
    }
}
