package at.plaz.flat_crawler.filters;

import at.plaz.flat_crawler.Maps;
import at.plaz.flat_crawler.ui.FilterPane;
import com.google.maps.model.TravelMode;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.ISODateTimeFormat;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Georg Plaz.
 */
public class DateTimeWrapper implements Comparable<DateTimeWrapper> {

    private final DateTime value;

    public DateTimeWrapper(DateTime value) {
        this.value = value;
    }

    @Override
    public int compareTo(DateTimeWrapper o) {
        return Long.compare(value.getMillis(), o.value.getMillis());
    }

    public DateTime getDateTime() {
        return value;
    }

    public static final void main(String... args){
        System.out.println(DateTimeFormat.forPattern("dd.MM.[yyyy]").parseDateTime("1.1.2016"));
        System.out.println(Maps.getTravelTime("Konrad-Peutingerstraße 21 81373 München", FilterPane.TUM_ADDRESS, TravelMode.BICYCLING));
    }

    @Override
    public String toString() {
        return "DateTimeWrapper{" +
                "value=" + value.toString(ISODateTimeFormat.dateTime()) +
                '}';
    }
}
