package at.plaz.flat_crawler.filters;

import at.plaz.flat_crawler.Flat;
import at.plaz.flat_crawler.FlatData;
import at.plaz.flat_crawler.value_retriever.FlatValueRetriever;
import com.google.appengine.repackaged.com.google.common.base.Converter;

/**
 * Created by Georg Plaz.
 */
public class HasValueFilter<A> extends AbstractFlatFilter {
    private final String valueName;
    private FlatValueRetriever<A> flatValueRetriever;

    public HasValueFilter(String valueName, Class<A> type) {
        this.valueName = valueName;
        flatValueRetriever = FlatData.getRetriever(valueName, type);
        setLabel("has: "+flatValueRetriever.getLabel());
    }

    public HasValueFilter(FlatValueRetriever<A> flatValueRetriever) {
        valueName = flatValueRetriever.getLabel();
        this.flatValueRetriever = flatValueRetriever;
        setLabel("has: "+flatValueRetriever.getLabel());
    }

    @Override
    public boolean isValid(Flat flat) {
        return flatValueRetriever.retrieve(flat) != null;
    }

    @Override
    public String toReadableString() {
        return "Has value: \""+valueName+"\"";
    }

    @Override
    public String toString() {
        return "HasValueFilter{" +
                "valueName='" + valueName + '\'' +
                '}';
    }
}
