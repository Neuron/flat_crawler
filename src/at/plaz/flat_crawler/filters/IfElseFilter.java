package at.plaz.flat_crawler.filters;

import at.plaz.flat_crawler.Flat;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class IfElseFilter extends ConditionalFlatFilter {
    private FlatFilter ifFilter;

    public IfElseFilter(FlatFilter ifFilter, FlatFilter thenFilter, FlatFilter elseFilter) {
        super(thenFilter, elseFilter);
        this.ifFilter = ifFilter;
    }

    @Override
    public String toReadableString() {
        return "if: (" + ifFilter + ") then: (" + getThenFilter().toReadableString() + ") else: (" + getElseFilter().toReadableString() + ").";
    }

    @Override
    public boolean conditionMet(Flat flat) {
        return ifFilter.isValid(flat);
    }

    public List<FlatFilter> subFilters() {
        return Arrays.asList(ifFilter, getThenFilter(), getElseFilter());
    }
}
