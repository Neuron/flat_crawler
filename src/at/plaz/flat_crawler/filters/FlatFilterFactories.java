package at.plaz.flat_crawler.filters;


import at.plaz.flat_crawler.value_retriever.FlatValueRetriever;
import at.plaz.flat_crawler.value_retriever.SimpleValueRetriever;
import com.google.appengine.repackaged.com.google.common.base.Converter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Georg Plaz.
 */
public class FlatFilterFactories {
//    private static Map<Class<?>, FlatFilterFactory> defaultFlatFilterFactories = new HashMap<>();
    private static Map<Class<?>, Converter<?, String>> defaultConverters = new HashMap<>();
//
    static{
        defaultConverters.put(Double.class, DoubleMinMaxFilter.getConverter());
        defaultConverters.put(DateTimeWrapper.class, DateFilter.getConverter());
    }

    @SuppressWarnings("unchecked")
    public static <A extends Comparable<A>> MinMaxFilterFactory<A> createDefaultMinMaxFilterFactory(
            FlatValueRetriever<A> valueRetriever, boolean validityIfMissing, Class<A> type){
        Converter<A, String> defaultConverter = (Converter<A, String>) defaultConverters.get(type);
        return new StandardMinMaxFilterFactory<>(valueRetriever, validityIfMissing, defaultConverter);
    }
}
