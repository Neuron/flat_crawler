package at.plaz.flat_crawler.filters;

import at.plaz.flat_crawler.Flat;
import at.plaz.flat_crawler.FlatData;
import at.plaz.flat_crawler.Value;
import at.plaz.flat_crawler.value_retriever.FlatValueRetriever;
import com.google.appengine.repackaged.com.google.common.base.Converter;

/**
 * Created by Georg Plaz.
 */
public class SimpleFlatFilter<A extends Comparable<A>> extends MinMaxFilter<A> {
    private FlatValueRetriever<A> flatValueRetriever;
    private String valueName;

    public SimpleFlatFilter(String valueName, A min, A max, boolean validityIfMissing, Class<A> type, Converter<A, String> converter) {
        super(min, max, validityIfMissing, converter);
        this.valueName = valueName;
        flatValueRetriever = FlatData.getRetriever(valueName, type);
        setLabel(flatValueRetriever.getLabel());
    }

    public SimpleFlatFilter(String valueName, boolean validityIfMissing, Class<A> type, Converter<A, String> converter) {
        this(valueName, null, null, validityIfMissing, type, converter);
    }

    public SimpleFlatFilter(FlatValueRetriever<A> flatValueRetriever, A min, A max, boolean validityIfMissing, Converter<A, String> converter) {
        super(min, max, validityIfMissing, converter);
        valueName = flatValueRetriever.getLabel();
        this.flatValueRetriever = flatValueRetriever;
        setLabel(flatValueRetriever.getLabel());
    }

    public SimpleFlatFilter(FlatValueRetriever<A> flatValueRetriever, boolean validityIfMissing, Converter<A, String> converter) {
        this(flatValueRetriever, null, null, validityIfMissing, converter);
    }

    @SuppressWarnings("unchecked")
    @Override
    public A getValue(Flat flat) {
        Value<A> value = flatValueRetriever.retrieve(flat);
        if(value.has()) {
            return value.get();
        }else{
            return null;
        }
    }

    @Override
    public String toString() {
        return "SimpleFlatFilter{" +
                "flatValueRetriever='" + flatValueRetriever + "\', " +
                "min='" + getMin() + "\', " +
                "max='" + getMax() + '\'' +
                '}';
    }

    @Override
    public String toReadableString() {
        return valueName + ", min: " + getMin() + ", " + getMax() + ".";
    }
}
