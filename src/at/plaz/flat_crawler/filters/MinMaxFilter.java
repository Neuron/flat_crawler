package at.plaz.flat_crawler.filters;

import at.plaz.flat_crawler.Flat;
import com.google.appengine.repackaged.com.google.common.base.Converter;

/**
 * Created by Georg Plaz.
 */
public abstract class MinMaxFilter<T extends Comparable<T>> extends AbstractFlatFilter{
    private T min;
    private T max;
    private boolean validityIfMissing;

    private Converter<T, String> stringConverter;

    public MinMaxFilter(T min, T max, boolean validityIfMissing, Converter<T, String> stringConverter) {
        this.min = min;
        this.max = max;
        this.validityIfMissing = validityIfMissing;
        this.stringConverter = stringConverter;
    }

//    public MinMaxFilter(T min, T max, String label, boolean validityIfMissing, Converter<T, String> stringConverter) {
//        super(label);
//        this.min = min;
//        this.max = max;
//        this.validityIfMissing = validityIfMissing;
//        this.stringConverter = stringConverter;
//    }

    public MinMaxFilter(boolean validityIfMissing, Converter<T, String> stringConverter) {
        this(null, null, validityIfMissing, stringConverter);
    }

    public abstract T getValue(Flat flat);

    @Override
    public boolean isValid(Flat flat) {
        T v = getValue(flat);
        return (v == null ? validityIfMissing :
                (min == null || min.compareTo(v) <= 0) && (max == null || v.compareTo(max) <= 0));
    }

    public void setMin(T min) {
        this.min = min;
    }

    public void setMax(T max) {
        this.max = max;
    }

    public T getMin() {
        return min;
    }

    public T getMax() {
        return max;
    }

    @Override
    public String toString() {
        return "MinMaxFilter{" +
                "min=" + min +
                ", max=" + max +
                '}';
    }

    public Converter<T, String> stringConverter(){
        return stringConverter;
    }
}
