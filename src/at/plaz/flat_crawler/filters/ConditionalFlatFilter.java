package at.plaz.flat_crawler.filters;

import at.plaz.flat_crawler.Flat;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public abstract class ConditionalFlatFilter extends AbstractFlatFilter {
    private FlatFilter thenFilter;
    private FlatFilter elseFilter;

    protected ConditionalFlatFilter(FlatFilter thenFilter, FlatFilter elseFilter) {
        this.thenFilter = thenFilter;
        this.elseFilter = elseFilter;
    }

    @Override
    public boolean isValid(Flat flat) {
        if (conditionMet(flat)){
            return thenFilter.isValid(flat);
        }else{
            return elseFilter.isValid(flat);
        }
    }

    public abstract boolean conditionMet(Flat flat);

    @Override
    public String toString() {
        return "ConditionalFlatFilter{" +
                "ifFilter=" + thenFilter +
                ", elseFilter=" + elseFilter +
                '}';
    }

    @Override
    public List<FlatFilter> subFilters() {
        return Arrays.asList(thenFilter, elseFilter);
    }

    public FlatFilter getThenFilter() {
        return thenFilter;
    }

    public FlatFilter getElseFilter() {
        return elseFilter;
    }


}
