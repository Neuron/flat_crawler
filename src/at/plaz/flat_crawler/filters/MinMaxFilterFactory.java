package at.plaz.flat_crawler.filters;

import at.plaz.flat_crawler.value_retriever.SimpleValueRetriever;

/**
 * Created by Georg Plaz.
 */
public interface MinMaxFilterFactory<A extends Comparable<A>> extends FlatFilterFactory{
    @Override
    MinMaxFilter<A> build();
}
