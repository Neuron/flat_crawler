package at.plaz.flat_crawler.filters;

/**
 * Created by Georg Plaz.
 */
public abstract class AbstractFlatFilter implements FlatFilter{
    private String label;

    public AbstractFlatFilter() {

    }

//    public AbstractFlatFilter(String label) {
//        setLabel(label);
//    }

    public String label() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
