package at.plaz.flat_crawler.filters;

import at.plaz.flat_crawler.Flat;

import java.util.Collections;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public interface FlatFilter {
    boolean isValid(Flat flat);

    String label();

    void setLabel(String label);

    default List<FlatFilter> subFilters(){
        return Collections.emptyList();
    }

    String toReadableString();

    String toString();
}
