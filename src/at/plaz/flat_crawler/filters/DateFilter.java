package at.plaz.flat_crawler.filters;

import at.plaz.flat_crawler.Util;
import at.plaz.flat_crawler.value_retriever.SimpleValueRetriever;
import com.google.appengine.repackaged.com.google.common.base.Converter;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/**
 * Created by Georg Plaz.
 */
public class DateFilter extends SimpleFlatFilter<DateTimeWrapper> {
    public static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("dd.MM.YYYY");
    private static Converter<DateTimeWrapper, String> converter = new Converter<DateTimeWrapper, String>() {
        @Override
        protected String doForward(DateTimeWrapper dateTimeWrapper) {
            return dateTimeWrapper.getDateTime().toString(FORMATTER);
        }

        @Override
        protected DateTimeWrapper doBackward(String s) {
            return new DateTimeWrapper(Util.cleanDateString(s));
        }
    };

    public static Converter<DateTimeWrapper, String> getConverter() {
        return converter;
    }

    public DateFilter(SimpleValueRetriever<DateTimeWrapper> retriever, DateTime min, DateTime max, boolean validityIfMissingDate) {
        super(retriever, new DateTimeWrapper(min), new DateTimeWrapper(max), validityIfMissingDate, converter);
    }

    public DateFilter(SimpleValueRetriever<DateTimeWrapper> retriever, boolean validityIfMissingDate) {
        super(retriever, new DateTimeWrapper(new DateTime()), new DateTimeWrapper(new DateTime()), validityIfMissingDate, converter);
    }

    public DateFilter(String valueName, DateTime min, DateTime max, boolean validityIfMissingDate) {
        super(valueName, new DateTimeWrapper(min), new DateTimeWrapper(max), validityIfMissingDate, DateTimeWrapper.class, converter);
    }

    public DateFilter(String valueName, boolean validityIfMissingDate) {
        super(valueName, new DateTimeWrapper(new DateTime()), new DateTimeWrapper(new DateTime()), validityIfMissingDate, DateTimeWrapper.class, converter);
    }

    @Override
    public String toString() {
        DateTime min = getMin().getDateTime();
        DateTime max = getMax().getDateTime();
        return "DateFilter{" +
                "min=" + min.toString(ISODateTimeFormat.dateTime()) +
                ", max=" + max.toString(ISODateTimeFormat.dateTime()) +
                '}';
    }


}
