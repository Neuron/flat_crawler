package at.plaz.flat_crawler.filters;

/**
 * Created by Georg Plaz.
 */
public interface FlatFilterFactory {
    FlatFilter build();
}
