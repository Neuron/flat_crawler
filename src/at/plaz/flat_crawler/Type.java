package at.plaz.flat_crawler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Georg Plaz.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)

public @interface Type {
    Class<?> value();
    String humanReadable();
    String name();
    int pos() default -1;
}
