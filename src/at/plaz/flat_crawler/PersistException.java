package at.plaz.flat_crawler;

/**
 * Created by Georg Plaz.
 */
public class PersistException extends Exception {
    public PersistException(Throwable e) {
        super(e);
    }
}
