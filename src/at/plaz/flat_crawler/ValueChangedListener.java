package at.plaz.flat_crawler;

/**
 * Created by Georg Plaz.
 */
public interface ValueChangedListener<A> {
    void valueChanged(A oldValue, A newValue);
}
