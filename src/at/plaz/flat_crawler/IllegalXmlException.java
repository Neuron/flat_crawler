package at.plaz.flat_crawler;

/**
 * Created by Georg Plaz.
 */
public class IllegalXmlException extends FlatData.ReadException {
    public IllegalXmlException(Exception e) {
        super(e);
    }
}
