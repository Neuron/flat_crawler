package at.plaz.flat_crawler.music;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class SimpleTakt implements Takt {
    private static final float EPSILON = 0.0000001f;
    private final NoteValue taktLength;
    private List<Tone> toneList;

    public SimpleTakt(Tone... tones) {
        this(NoteValue.SEMIBREVE, tones);
    }

    public SimpleTakt(NoteValue taktLength, Tone... tones) {
        this.taktLength = taktLength;
        toneList = Arrays.asList(tones);
        float length = 0;
        for (Tone tone : tones) {
            length += tone.getNoteValue().getRelativeLength();
        }
        if (length > taktLength.getRelativeLength() + EPSILON || length < taktLength.getRelativeLength() - EPSILON) {
            throw new RuntimeException("Takt doesn't have the appropriate length. expected relative length: " +
                    taktLength.getRelativeLength()+ ". Actual combined tone length: "+ length + ". Tones: "+toneList);
        }
    }

    @Override
    public NoteValue length() {
        return taktLength;
    }

    @Override
    public Iterator<Tone> iterator() {
        return toneList.iterator();
    }
}
