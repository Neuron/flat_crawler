package at.plaz.flat_crawler.music;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class TaktMusic extends Music {
    private float taktLength;
    private List<Takt> taktList = new LinkedList<>();
    public TaktMusic(int standardToneLength) {
        super(standardToneLength);
    }

    @Override
    public void addTone(Note note, NoteValue noteValue, double volume) {
        super.addTone(note, noteValue, volume);
    }
}
