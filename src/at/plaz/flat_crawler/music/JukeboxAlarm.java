package at.plaz.flat_crawler.music;

import javax.sound.sampled.LineUnavailableException;

/**
 * Created by Georg Plaz.
 */
public class JukeboxAlarm implements Alarm {
    private Music music;

    public JukeboxAlarm(Music music) {
        this.music = music;
    }

    @Override
    public void play() {
        try {
            Jukebox jukebox = new Jukebox();
            jukebox.play(music);
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }
}
