package at.plaz.flat_crawler.music;

/**
 * Created by Georg Plaz.
 */
public interface Alarm {
    void play();
}
