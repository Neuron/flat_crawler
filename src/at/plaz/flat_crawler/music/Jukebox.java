package at.plaz.flat_crawler.music;

import com.sun.org.apache.regexp.internal.RE;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import static at.plaz.flat_crawler.music.Note.*;
import static at.plaz.flat_crawler.music.Note.C_4;
import static at.plaz.flat_crawler.music.Note.D_4;
import static at.plaz.flat_crawler.music.NoteValue.*;

/**
 * Created by Georg Plaz.
 */
public class Jukebox {
    public static final int DEFAULT_TONE_DURATION = 1000;
    private SourceDataLine sdl;

    public void open() throws LineUnavailableException {
        AudioFormat af = new AudioFormat(
                Music.SAMPLE_RATE, // sampleRate
                8,           // sampleSizeInBits
                1,           // channels
                true,        // signed
                true);
        sdl = AudioSystem.getSourceDataLine(af);
        sdl.open(af, Music.SAMPLE_RATE);
        sdl.start();
    }

    public boolean isOpen(){
        return sdl != null;
    }

    public void tone(Tone tone, long toneDuration) throws LineUnavailableException {
        tone(tone.render(toneDuration));
    }

    public void tone(SoundBite soundBite) throws LineUnavailableException {
        sdl.write(soundBite.getData(), 0, soundBite.getData().length);
        sdl.write(Music.MINI_PAUSE.getData(), 0, Music.MINI_PAUSE.getData().length);
    }

    public static void main(String... args){
        Music tone = new Music();
        tone.addTone(new Tone(WTF_BEEP, NoteValue.QUAVER));
        Jukebox jukebox = new Jukebox();
        try {
            jukebox.play(tone);
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    public void close(){
        sdl.drain();
//        sdl.stop();
        sdl.close();
        sdl = null;
    }

    public static Music zelda(){
        Music sariasSong = new Music(1500);
        sariasSong.addTone(F_4, QUAVER);
        sariasSong.addTone(F_4.higher(2), QUAVER);
        sariasSong.addTone(F_4.higher(3), CROTCHET);
        sariasSong.addTone(F_4, QUAVER);
        sariasSong.addTone(F_4.higher(2), QUAVER);
        sariasSong.addTone(F_4.higher(3), CROTCHET);

        sariasSong.addTone(F_4, QUAVER);
        sariasSong.addTone(F_4.higher(2), QUAVER);
        sariasSong.addTone(F_4.higher(3), QUAVER);
        sariasSong.addTone(F_4.higher(6), QUAVER);
        sariasSong.addTone(F_4.higher(5), CROTCHET);
        sariasSong.addTone(F_4.higher(3), QUAVER);
        sariasSong.addTone(F_4.higher(4), QUAVER);

        sariasSong.addTone(F_4.higher(3), QUAVER);
        sariasSong.addTone(F_4.higher(1), QUAVER);
        sariasSong.addTone(F_4.higher(-1), MINIM);
        sariasSong.addTone(REST, QUAVER);
        sariasSong.addTone(D_4, QUAVER);

        sariasSong.addTone(D_4.higher(1), QUAVER);
        sariasSong.addTone(D_4.higher(3), QUAVER);
        sariasSong.addTone(D_4.higher(1), MINIM);
        sariasSong.addTone(REST, CROTCHET);

        sariasSong.addTone(F_4, QUAVER);
        sariasSong.addTone(F_4.higher(2), QUAVER);
        sariasSong.addTone(F_4.higher(3), CROTCHET);
        sariasSong.addTone(F_4, QUAVER);
        sariasSong.addTone(F_4.higher(2), QUAVER);
        sariasSong.addTone(F_4.higher(3), CROTCHET);

        sariasSong.addTone(F_4, QUAVER);
        sariasSong.addTone(F_4.higher(2), QUAVER);
        sariasSong.addTone(F_4.higher(3), QUAVER);
        sariasSong.addTone(F_4.higher(6), QUAVER);
        sariasSong.addTone(F_4.higher(5), CROTCHET);
        sariasSong.addTone(F_4.higher(3), QUAVER);
        sariasSong.addTone(F_4.higher(4), QUAVER);

        sariasSong.addTone(F_4.higher(6), QUAVER);
        sariasSong.addTone(F_4.higher(3), QUAVER);
        sariasSong.addTone(F_4.higher(1), MINIM);
        sariasSong.addTone(REST, QUAVER);
        sariasSong.addTone(F_4.higher(3), QUAVER);

        sariasSong.addTone(F_4.higher(1), QUAVER);
        sariasSong.addTone(F_4.higher(1-3), QUAVER);
        sariasSong.addTone(F_4.higher(1-2), MINIM);
        sariasSong.addTone(REST, CROTCHET);

        sariasSong.addTone(D_4, QUAVER);
        sariasSong.addTone(D_4.higher(1), QUAVER);
        sariasSong.addTone(D_4.higher(2), CROTCHET);
        sariasSong.addTone(D_4.higher(3), QUAVER);
        sariasSong.addTone(D_4.higher(4), QUAVER);
        sariasSong.addTone(D_4.higher(5), CROTCHET);

        sariasSong.addTone(D_4.higher(6), QUAVER);
        sariasSong.addTone(D_4.higher(5), QUAVER);
        sariasSong.addTone(D_4.higher(1), MINIM);
        sariasSong.addTone(REST, CROTCHET);
//
        sariasSong.addTone(D_4, QUAVER);
        sariasSong.addTone(D_4.higher(1), QUAVER);
        sariasSong.addTone(D_4.higher(2), CROTCHET);
        sariasSong.addTone(D_4.higher(3), QUAVER);
        sariasSong.addTone(D_4.higher(4), QUAVER);
        sariasSong.addTone(D_4.higher(5), CROTCHET);

        sariasSong.addTone(D_4.higher(6), QUAVER);
        sariasSong.addTone(D_4.higher(7), QUAVER);
        sariasSong.addTone(D_4.higher(8), MINIM);
        sariasSong.addTone(REST, CROTCHET);

        sariasSong.addTone(D_4, QUAVER);
        sariasSong.addTone(D_4.higher(1), QUAVER);
        sariasSong.addTone(D_4.higher(2), CROTCHET);
        sariasSong.addTone(D_4.higher(3), QUAVER);
        sariasSong.addTone(D_4.higher(4), QUAVER);
        sariasSong.addTone(D_4.higher(5), CROTCHET);

        sariasSong.addTone(D_4.higher(6), QUAVER);
        sariasSong.addTone(D_4.higher(5), QUAVER);
        sariasSong.addTone(D_4.higher(1), MINIM);
        sariasSong.addTone(REST, CROTCHET);

        sariasSong.addTone(D_4, QUAVER);
        sariasSong.addTone(D_4.lower(1), QUAVER);
        for (int i = 0; i < 3; i++) {
            sariasSong.addTone(F_4.higher(i), QUAVER);
            sariasSong.addTone(F_4.higher(i).lower(1), QUAVER);
        }

//
//            music.addTone(D_4, QUAVER);
//            music.addTone(D_4.lower(1), QUAVER);
//            music.addTone(F_4, QUAVER);
//            music.addTone(F_4.lower(1), QUAVER);
//            music.addTone(G_4, QUAVER);
//            music.addTone(G_4.lower(1), QUAVER);
//            music.addTone(A_4, QUAVER);
//            music.addTone(A_4.lower(1), QUAVER);

        sariasSong.addTone(B_4, QUAVER);
        sariasSong.addTone(B_4.lower(1), QUAVER);
        sariasSong.addTone(C_5, QUAVER);
        sariasSong.addTone(C_5.lower(1), QUAVER);
        sariasSong.addTone(D_5, QUAVER);
        sariasSong.addTone(D_5.lower(1), QUAVER);
        sariasSong.addTone(D_5.lower(2).higher(3), SEMIQUAVER);
        sariasSong.addTone(D_5.lower(1).higher(3), QUAVER);
        sariasSong.addTone(D_5.lower(3).higher(3), SEMIQUAVER);
        sariasSong.addTone(D_5.lower(2).higher(3), SEMIBREVE);

        sariasSong.addTone(REST, MINIM);
        sariasSong.addTone(REST, CROTCHET);
        sariasSong.addTone(B_5.lower(4), QUAVER);
        sariasSong.addTone(REST, QUAVER);
        return sariasSong;
    }

    public static void playZelda() {
        try {
            Music music = zelda();

            Jukebox jukebox = new Jukebox();
            jukebox.play(music);
            System.out.println("run 1");
            jukebox.play(music);
            System.out.println("run 2");
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    public static void playAlleMeineEntchen() {
        try {
            Music music = new Music(1000);
            music.addTone(C_4, CROTCHET);
            music.addTone(D_4, CROTCHET);
            music.addTone(E_4, CROTCHET);
            music.addTone(F_4, CROTCHET);
            music.addTone(G_4, MINIM);
            music.addTone(G_4, MINIM);
            music.addTone(A_4, CROTCHET);
            music.addTone(A_4, CROTCHET);
            music.addTone(A_4, CROTCHET);
            music.addTone(A_4, CROTCHET);
            music.addTone(G_4, SEMIBREVE);

            music.addTone(A_4, CROTCHET);
            music.addTone(A_4, CROTCHET);
            music.addTone(A_4, CROTCHET);
            music.addTone(A_4, CROTCHET);
            music.addTone(G_4, SEMIBREVE);
            music.addTone(F_4, CROTCHET);
            music.addTone(F_4, CROTCHET);
            music.addTone(F_4, CROTCHET);
            music.addTone(F_4, CROTCHET);
            music.addTone(E_4, MINIM);
            music.addTone(E_4, MINIM);

            music.addTone(D_4, CROTCHET);
            music.addTone(D_4, CROTCHET);
            music.addTone(D_4, CROTCHET);
            music.addTone(D_4, CROTCHET);
            music.addTone(C_4, SEMIBREVE);
            Jukebox jukebox = new Jukebox();
            jukebox.play(music);
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    public void play(Music music) throws LineUnavailableException {
        if(music.hasToneDurationRecommendation()){
            long toneDuration = music.getToneDurationRecommendation();
            play(music, toneDuration);
        }else{
            play(music, DEFAULT_TONE_DURATION);
        }
    }

    public void play(Music music, long toneDuration) throws LineUnavailableException {
        boolean wasOpen = isOpen();
        if(!wasOpen){
            open();
        }
        for (Tone tone : music){
            tone(tone, toneDuration);
        }
        if(!wasOpen){
            close();
        }
    }

    public static Music alarm() {
        Music music = new Music(1000);
        for (int i = 0; i < 25; i++) {
            music.addTone(HIGH_BEEP, SEMIQUAVER);
            music.addTone(VERY_HIGH_BEEP, SEMIQUAVER);
        }
        return music;
    }

//    public void play(Music music, float volumeModifier) throws LineUnavailableException {
//        boolean wasOpen = isOpen();
//        if(!wasOpen){
//            open();
//        }
//        for (Tone tone : music){
//            tone(tone.modifyVolume(volumeModifier));
//        }
//        if(!wasOpen){
//            close();
//        }
//    }
}
