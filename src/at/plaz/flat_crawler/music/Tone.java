package at.plaz.flat_crawler.music;

/**
 * Created by Georg Plaz.
 */
public class Tone {
    private Note note;
    private NoteValue noteValue;
    private double volume;

    public Tone(Note note, NoteValue noteValue) {
        this(note, noteValue, 1);
    }

    public Tone(Note note, NoteValue noteValue, double volume) {
        this.note = note;
        this.noteValue = noteValue;
        this.volume = Math.min(1, Math.max(volume, 0));

    }

    public Tone(Note note) {
        this(note, NoteValue.SEMIBREVE);
    }

    public SoundBite render(long noteDuration) {
        return renderToAbsoluteDuration((long)(noteDuration * noteValue.getRelativeLength()));
    }

    public SoundBite renderToAbsoluteDuration(long absoluteDuration) {
        return new SoundBite(note.getHz(), absoluteDuration, volume);
    }

    public NoteValue getNoteValue() {
        return noteValue;
    }

    public double getVolume() {
        return volume;
    }

    public Note getNote() {
        return note;
    }

    @Override
    public String toString() {
        return "Tone{" +
                "note=" + note +
                ", noteValue=" + noteValue +
                ", volume=" + volume +
                '}';
    }

    public Tone modifyVolume(float volumeModifier) {
        return new Tone(note, noteValue, Math.min(1, volumeModifier * volume));
    }
}

