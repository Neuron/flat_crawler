package at.plaz.flat_crawler.music;

import javax.sound.sampled.LineUnavailableException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static at.plaz.flat_crawler.music.Note.REST;

/**
 * Created by Georg Plaz.
 */
public class Music implements Iterable<Tone>{
    public static int SAMPLE_RATE = 16 * 1024;
    private List<Tone> tones = new LinkedList<>();
    public static final SoundBite MINI_PAUSE = new Tone(REST).render(20);
    private long toneDurationRecommendation;

    public Music() {
        toneDurationRecommendation = -1;
    }

    public Music(int standardToneLength) {
        this.toneDurationRecommendation = standardToneLength;
    }

    public void addTone(Tone tone){
        tones.add(tone);
    }

    public void addTone(Note note, NoteValue noteValue) {
        addTone(note, noteValue, 1);
    }

    public void addTone(Note note, NoteValue noteValue, double volume) {
        addTone(new Tone(note, noteValue, volume));
    }

    @Override
    public Iterator<Tone> iterator() {
        return tones.iterator();
    }

    public long getToneDurationRecommendation() {
        return toneDurationRecommendation;
    }

    public boolean hasToneDurationRecommendation() {
        return toneDurationRecommendation != -1;
    }
}
