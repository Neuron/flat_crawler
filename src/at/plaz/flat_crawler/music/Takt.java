package at.plaz.flat_crawler.music;

/**
 * Created by Georg Plaz.
 */
public interface Takt extends Iterable<Tone>{
    NoteValue length();
}
