package at.plaz.flat_crawler.music;

/**
 * Created by Georg Plaz.
 */
public enum Note {
    REST(0), C_4(261.63), D_4(293.66), E_4(329.63), F_4(349.23), G_4(392.00), A_4(440.00), B_4(493.88),
    C_5(523.25), D_5(587.33), E_5(659.25), F_5(698.46), G_5(783.99), A_5(880.00), B_5(987.77),

    HIGH_BEEP(800),
    VERY_HIGH_BEEP(1000),
    WTF_BEEP(12000);

    private double v;

    public Note higher(){
        return higher(1);
    }

    public Note higher(int by){
        return values()[snip(ordinal() + by)];
    }

    public Note lower(){
        return lower(1);
    }

    public Note lower(int by){
        return values()[snip(ordinal() - by)];
    }

    private int snip(int by){
        return Math.min(HIGH_BEEP.ordinal() - 1, Math.max(1, by));
    }
    Note(double v) {
        this.v = v;
    }

    public double getHz() {
        return v;
    }
}
