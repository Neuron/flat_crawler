package at.plaz.flat_crawler.music;

/**
 * Created by Georg Plaz.
 */
public class SoundBite {
    private byte[] data;
    private double volume;

    public SoundBite(double hz, long duration, double volume) {
        data = new byte[(int)((duration/1000.) * Music.SAMPLE_RATE)];
        for (int i = 0; i < data.length; i++) {
            double angle = i / (Music.SAMPLE_RATE / hz) * 2.0 * Math.PI;
            data[i] = (byte)(Math.sin(angle) * 127.0 * volume);
        }
    }
    public SoundBite(byte[] data, double volume) {
        this.data = data;
        this.volume = volume;
    }

    public byte[] getData() {
        return data;
    }

    public SoundBite modifyVolume(float volumeModifier){
        double newVolume = volumeModifier * volume;
        if(newVolume > 1){
            newVolume = 1;
            volumeModifier /= newVolume;
        }
        byte[] newData = new byte[data.length];
        for (int i = 0; i < data.length; i++) {
            newData[i] = (byte) (data[i] * volumeModifier);
        }
        return new SoundBite(newData, newVolume);
    }
}
