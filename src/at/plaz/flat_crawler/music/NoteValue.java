package at.plaz.flat_crawler.music;

/**
 * Created by Georg Plaz.
 */
public class NoteValue {
    public static final NoteValue MAXIMA = new NoteValue(8, "Maxima");
    public static final NoteValue LONGA = new NoteValue(4, "Longa");
    public static final NoteValue BREVE = new NoteValue(2, "Breve");
    public static final NoteValue SEMIBREVE = new NoteValue(1, "Semibreve");
    public static final NoteValue MINIM = new NoteValue(1f/2, "Minim");
    public static final NoteValue CROTCHET = new NoteValue(1f/4, "Crochet");
    public static final NoteValue QUAVER = new NoteValue(1f/8, "Quaver");
    public static final NoteValue SEMIQUAVER = new NoteValue(1f/16, "Semiquaver");

    private float relativeLength;
    private String name;

    NoteValue(float relativeLength, String name) {
        this.relativeLength = relativeLength;
        this.name = name;
    }

    public float getRelativeLength() {
        return relativeLength;
    }

    @Override
    public String toString() {
        return "NoteValue{" +
                "name=\"" + name + "\", " +
                "relativeLength=" + relativeLength +
                '}';
    }
}
