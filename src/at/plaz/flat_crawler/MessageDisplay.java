package at.plaz.flat_crawler;

/**
 * Created by Georg Plaz.
 */
public interface MessageDisplay {
    String ERROR_TAG = "error";
    String INFO_TAG = "info";
    String DEBUG_TAG = "debug";
    default void display(String message){
        display(new Message(message, INFO_TAG));
    }
    default void display(String message, String... tags){
        display(new Message(message, tags));
    }
    void display(Message message);

    default void sleepAndPrint(long duration){
        try {
            String tag = duration > 5000 ? INFO_TAG : DEBUG_TAG;
            display("sleeping "+ Math.round(duration/1000.)+" secs...", tag);
            Thread.sleep(duration);
            display("woke up!", tag);
        } catch (InterruptedException e) {
            display("woke up prematurely!", ERROR_TAG);
            e.printStackTrace();
        }
    }
}
