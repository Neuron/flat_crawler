package at.plaz.flat_crawler;

import at.plaz.flat_crawler.crawler.SiteType;
import at.plaz.flat_crawler.filters.DateTimeWrapper;
import at.plaz.flat_crawler.value_retriever.SimpleValueRetriever;
import com.google.appengine.repackaged.com.google.common.collect.HashBasedTable;
import com.google.appengine.repackaged.com.google.common.collect.Table;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by Georg Plaz.
 */
public class FlatData {
    public static final int TITLE_VALUE_POS = 0;
    public static final int ID_VALUE_POS = 1;
    public static final int URL_VALUE_POS = 2;
    public static final int RENT_COLD_VALUE_POS = 3;
    public static final int DEPOSIT_VALUE_POS = 4;
    public static final int TOTAL_RENT_VALUE_POS = 5;
    public static final int QUADRATIC_METER_VALUE_POS = 6;
    public static final int ADDRESS_VALUE_VALUE_POS = 7;
    public static final int FREE_FROM_VALUE_POS = 8;
    public static final int ROOM_COUNT_VALUE_POS = 9;
    public static final int SITE_TYPE_VALUE_POS = 10;

    public static final String QUADRATIC_METER_VALUE_NAME = "quadrat_meter";
    public static final String TITLE_VALUE_NAME = "title";
    public static final String URL_VALUE_NAME = "site";
    public static final String RENT_COLD_VALUE_NAME = "rent_cold";
    public static final String TOTAL_RENT_VALUE_NAME = "total_rent";
    public static final String ADDRESS_VALUE_NAME = "address";
    public static final String ROOM_COUNT_VALUE_NAME = "room_count";
    public static final String SIDE_COST_VALUE_NAME = "side_cost";
    public static final String HEATING_COST_VALUE_NAME = "heating_cost";
    public static final String DESCRIPTION_VALUE_NAME = "description";
    public static final String PRICE_INFO_VALUE_NAME = "price_info";
    public static final String OWNER_NAME_VALUE_NAME = "owner_name";
    public static final String OWNER_DESCRIPTION_VALUE_NAME = "owner_description";
    public static final String OWNER_PHONE_NUMBER_VALUE_NAME = "owner_phone_number";
    public static final String OWNER_EMAIL_VALUE_NAME = "owner_email";
    public static final String FREE_FROM_VALUE_NAME = "free_from";
    public static final String INVENTORY_VALUE_NAME = "inventory";
    public static final String LOCATION_VALUE_NAME = "location";
    public static final String OTHER_INFO_VALUE_NAME = "other_info";
    public static final String SITE_TYPE_VALUE_NAME = "site_type";
    public static final String ID_VALUE_NAME = "id";
    public static final String DATE_DISCOVERED_NAME = "date_discovered";
    public static final String DATE_EDITED_NAME = "date_edited";
    public static final String OWNER_COMPANY_VALUE_NAME = "owner_company";
    public static final String OWNER_ADDRESS_VALUE_NAME = "owner_address";
    
    public static final String FLAT_XML_TAG = "flat";
    public static final String NAME_XML_ATTR = "name";
    private static final String DATE_CREATED_VALUE_NAME = "date_created";
    private static final String DEPOSIT_VALUE_NAME = "deposit";

    private static Map<String, SimpleValueRetriever<?>> flatValueRetrieverMap = new LinkedHashMap<>();
    private static Table<Class<?>, String, SimpleValueRetriever<?>> flatValueRetrieversByClass = HashBasedTable.create();
    private static Set<Method> dataMethods = new LinkedHashSet<>();

    public static SimpleValueRetriever<?> getRetriever(String label){
        return flatValueRetrieverMap.get(label);
    }

    @SuppressWarnings("unchecked")
    public static <A> SimpleValueRetriever<A> getRetriever(String label, Class<A> type){
        return (SimpleValueRetriever<A>) flatValueRetrieversByClass.get(type, label);
    }

    public static Collection<String> flatValueLabels(){
        return flatValueRetrieverMap.keySet();
    }

    static {
        Set<Method> noPosMethods = new LinkedHashSet<>();
        Method[] orderedMethods = new Method[FlatData.class.getDeclaredMethods().length];
        for (Method method : FlatData.class.getDeclaredMethods()) {
            if (method.isAnnotationPresent(Type.class)) {
                Type annotation = method.getDeclaredAnnotation(Type.class);
                int pos = annotation.pos();
                if(pos != -1){
                    if(orderedMethods[pos] != null){
                        throw new RuntimeException("Position "+pos+" used multiple times for method in "+FlatData.class.getName()+" .");
                    }else{
                        orderedMethods[pos] = method;
                    }
                }else{
                    noPosMethods.add(method);
                }
            }
        }
        for(Method method : orderedMethods){
            addMethod(method);
        }
        for(Method method : noPosMethods){
            addMethod(method);
        }
    }

    private static void addMethod(Method method){
        if(method != null) {
            dataMethods.add(method);
            Type annotation = method.getDeclaredAnnotation(Type.class);
            SimpleValueRetriever flatValueRetriever = new SimpleValueRetriever<>(method, annotation.humanReadable(), annotation.value());
            if(!flatValueRetrieverMap.containsKey(annotation.name())){
                flatValueRetrieverMap.put(annotation.name(), flatValueRetriever);
                flatValueRetrieversByClass.put(flatValueRetriever.getType(), annotation.name(), flatValueRetriever);
            }else{
                throw new RuntimeException();
            }
        }
    }

    private Value<Double> quadraticMeter = new Value<>();
    private Value<String> title = new Value<>();
    private Value<String> site = new Value<>();
    private Value<Double> rentCold = new Value<>();
    private Value<Double> totalRent = new Value<>();
    private Value<Double> deposit = new Value<>();
    private Value<String> address = new Value<>();

    private Value<Double> roomCount = new Value<>();
    private Value<Double> sideCost = new Value<>();
    private Value<Double> heatingCost = new Value<>();
    private Value<String> description = new Value<>();
    private Value<String> inventory = new Value<>();
    private Value<String> priceInfo = new Value<>();

    private Value<String> ownerName = new Value<>();
    private Value<String> ownerDescription = new Value<>();
    private Value<String> ownerNumber = new Value<>();
    private Value<String> ownerEmail = new Value<>();

    private Value<DateTimeWrapper> availableFrom = new Value<>();
    private Value<String> location = new Value<>();
    private Value<String> otherInfo = new Value<>();
    private Value<SiteType> siteType = new Value<>();
    private Value<String> id = new Value<>();
    private Value<DateTimeWrapper> dateDiscovered = new Value<>();
    private Value<DateTimeWrapper> dateCreated = new Value<>();
    private Value<String> ownerCompany = new Value<>();
    private Value<String> ownerAddress = new Value<>();
    private Value<DateTimeWrapper> lastEdited = new Value<>();

    @Type(value = Double.class, humanReadable = "quadratic meters", name = QUADRATIC_METER_VALUE_NAME, pos = QUADRATIC_METER_VALUE_POS)
    public Value<Double> getQuadraticMeter() {
        return quadraticMeter;
    }

    @Type(value = String.class, humanReadable = "flat title", name = TITLE_VALUE_NAME, pos = TITLE_VALUE_POS)
    public Value<String> getTitle() {
        return title;
    }

    @Type(value = String.class, humanReadable = "link to flat", name = URL_VALUE_NAME, pos = URL_VALUE_POS)
    public Value<String> getSite() {
        return site;
    }

    @Type(value = Double.class, humanReadable = "rent excluding heating costs", name = RENT_COLD_VALUE_NAME, pos = RENT_COLD_VALUE_POS)
    public Value<Double> getRentCold() {
        return rentCold;
    }

    @Type(value = Double.class, humanReadable = "total rent", name = TOTAL_RENT_VALUE_NAME, pos = TOTAL_RENT_VALUE_POS)
    public Value<Double> getTotalRent() {
        return totalRent;
    }

    @Type(value = Double.class, humanReadable = "deposit", name = DEPOSIT_VALUE_NAME, pos = DEPOSIT_VALUE_POS)
    public Value<Double> getDeposit() {
        return deposit;
    }

    @Type(value = String.class, humanReadable = "flat address", name = ADDRESS_VALUE_NAME, pos = ADDRESS_VALUE_VALUE_POS)
    public Value<String> getAddress() {
        return address;
    }

    @Type(value = Double.class, humanReadable = "number of rooms", name = ROOM_COUNT_VALUE_NAME, pos = ROOM_COUNT_VALUE_POS)
    public Value<Double> getRoomCount() {
        return roomCount;
    }

    @Type(value = Double.class, humanReadable = "extra costs and fees", name = SIDE_COST_VALUE_NAME)
    public Value<Double> getSideCost() {
        return sideCost;
    }

    @Type(value = Double.class, humanReadable = "heating costs", name = HEATING_COST_VALUE_NAME)
    public Value<Double> getHeatingCost() {
        return heatingCost;
    }

    @Type(value = String.class, humanReadable = "flat description", name = DESCRIPTION_VALUE_NAME)
    public Value<String> getDescription() {
        return description;
    }

    @Type(value = String.class, humanReadable = "price info", name = PRICE_INFO_VALUE_NAME)
    public Value<String> getPriceInfo() {
        return priceInfo;
    }

    @Type(value = String.class, humanReadable = "name of owner", name = OWNER_NAME_VALUE_NAME)
    public Value<String> getOwnerName() {
        return ownerName;
    }

    @Type(value = String.class, humanReadable = "about owner", name = OWNER_DESCRIPTION_VALUE_NAME)
    public Value<String> getOwnerDescription() {
        return ownerDescription;
    }

    @Type(value = String.class, humanReadable = "owner company", name = OWNER_COMPANY_VALUE_NAME)
    public Value<String> getOwnerCompany() {
        return ownerCompany;
    }

    @Type(value = String.class, humanReadable = "owner address", name = OWNER_ADDRESS_VALUE_NAME)
    public Value<String> getOwnerAddress() {
        return ownerAddress;
    }

    @Type(value = String.class, humanReadable = "owner phone number", name = OWNER_PHONE_NUMBER_VALUE_NAME)
    public Value<String> getOwnerNumber() {
        return ownerNumber;
    }

    @Type(value = String.class, humanReadable = "owner e-mail", name = OWNER_EMAIL_VALUE_NAME)
    public Value<String> getOwnerEmail() {
        return ownerEmail;
    }

    @Type(value = DateTimeWrapper.class, humanReadable = "flat available from", name = FREE_FROM_VALUE_NAME, pos = FREE_FROM_VALUE_POS)
    public Value<DateTimeWrapper> getAvailableFrom() {
        return availableFrom;
    }

    @Type(value = String.class, humanReadable = "inventory info", name = INVENTORY_VALUE_NAME)
    public Value<String> getInventory() {
        return inventory;
    }

    @Type(value = String.class, humanReadable = "location description", name = LOCATION_VALUE_NAME)
    public Value<String> getLocationDescription() {
        return location;
    }

    @Type(value = String.class, humanReadable = "other info", name = OTHER_INFO_VALUE_NAME)
    public Value<String> getOtherInfo() {
        return otherInfo;
    }

    @Type(value = SiteType.class, humanReadable = "site", name = SITE_TYPE_VALUE_NAME, pos = SITE_TYPE_VALUE_POS)
    public Value<SiteType> getSiteType() {
        return siteType;
    }

    @Type(value = String.class, humanReadable = "site specific flat id", name = ID_VALUE_NAME, pos = ID_VALUE_POS)
    public Value<String> getId() {
        return id;
    }

    @Type(value = DateTimeWrapper.class, humanReadable = "flat discovery date", name = DATE_DISCOVERED_NAME)
    public Value<DateTimeWrapper> getDateDiscovered() {
        return dateDiscovered;
    }

    @Type(value = DateTimeWrapper.class, humanReadable = "flat creation date", name = DATE_CREATED_VALUE_NAME)
    public Value<DateTimeWrapper> getDateCreated() {
        return dateCreated;
    }

    @Type(value = DateTimeWrapper.class, humanReadable = "date last edited", name = DATE_EDITED_NAME)
    public Value<DateTimeWrapper> getLastEdited() {
        return lastEdited;
    }


    public static class ReadException extends Exception{
        public ReadException(Exception e) {
            super(e);
        }

        public ReadException() {
        }

        public ReadException(String s) {
            super(s);
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("FlatData{");
        Class<Type> annotationClass = Type.class;
        for (Method method : dataMethods){
            try {
                Value v = (Value) method.invoke(this);
                if(v.has()) {
                    Type annotation = method.getDeclaredAnnotation(annotationClass);
                    Class<?> valueType = annotation.value();
                    String name = annotation.name();
                    String className = valueType.getSimpleName();
                    stringBuilder.append("data{[").append(className).append("] - ").append(name).append("=");
                    if (valueType == String.class){
                        stringBuilder.append("\"").append(String.valueOf(v.get())).append("\"");
                    } else if(valueType == Double.class || valueType == SiteType.class) {
                        stringBuilder.append(String.valueOf(v.get()));
                    }else if(valueType == DateTimeWrapper.class){
                        @SuppressWarnings("unchecked")
                        Value<DateTimeWrapper> value = v;
                        DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
                        stringBuilder.append(fmt.print(value.get().getDateTime()));
                    }else{
                        throw new RuntimeException();
                    }
                    stringBuilder.append("}");
                }
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    public static Collection<SimpleValueRetriever<?>> getFlatValueRetrievers(){
        return flatValueRetrieverMap.values();
    }

    @SuppressWarnings("unchecked")
    public static <A> Collection<SimpleValueRetriever<A>> getFlatValueRetrievers(Class<A> type){
        Collection<SimpleValueRetriever<A>> toReturn = new LinkedList<>();
        for(SimpleValueRetriever<?> simpleValueRetriever : flatValueRetrieversByClass.row(type).values()){
            toReturn.add((SimpleValueRetriever<A>) simpleValueRetriever);
        }
        return toReturn;
    }

//    static{
//        for (Method method : getAnnotatedMethods()){
//            Class<Type> annotationClass = Type.class;
//            Type annotation = method.getDeclaredAnnotation(annotationClass);
//            flatValueRetrievers.add(new SimpleValueRetriever<>(method, annotation.name()));
//        }
//    }
}
