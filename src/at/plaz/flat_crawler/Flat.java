package at.plaz.flat_crawler;

import at.plaz.flat_crawler.crawler.SiteType;
import at.plaz.flat_crawler.filters.DateTimeWrapper;
import at.plaz.flat_crawler.value_retriever.FlatValueRetriever;
import at.plaz.flat_crawler.value_retriever.SimpleValueRetriever;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static at.plaz.flat_crawler.FlatData.*;

/**
 * Created by Georg Plaz.
 */
public class Flat {
    private FlatData flatData;

    public Flat(FlatData flatData) {
        this.flatData = flatData;
    }

    public Flat() {
        this(new FlatData());
    }

    public FlatData getFlatData() {
        return flatData;
    }

    @Override
    public String toString() {
        return "Flat{" +
                "flatData=" + flatData +
                '}';
    }

    public void persist() throws PersistException {
        try {
            Document doc = XmlUtil.createDocument();
            Element rootElement = doc.createElement(FLAT_XML_TAG);
            doc.appendChild(rootElement);

            for (SimpleValueRetriever valueRetriever : FlatData.getFlatValueRetrievers()){
                Value v = valueRetriever.retrieve(this);
                if(v.has()) {
                    Class<?> valueType = valueRetriever.getType();
                    String valueName = valueRetriever.getValueName();
                    String className = valueType.getSimpleName();
                    Element typeElement = doc.createElement(className);
                    typeElement.setAttribute(NAME_XML_ATTR, valueName);
                    rootElement.appendChild(typeElement);

                    if (valueType == String.class || valueType == Float.class || valueType == Double.class || valueType == SiteType.class) {
                        typeElement.setTextContent(String.valueOf(v.get()));
                    }else if(valueType == DateTimeWrapper.class){
                        @SuppressWarnings("unchecked")
                        Value<DateTimeWrapper> value = v;
                        DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
                        typeElement.setTextContent(fmt.print(value.get().getDateTime()));
                    }
                }
            }
            File folder = new File(getFlatData().getSiteType().get().getShortName()+"/"+getFlatData().getId().get());
            folder.mkdirs();
            File file = new File(folder, "flat.xml");
            XmlUtil.persist(doc, file);
        } catch (ParserConfigurationException e) {
            throw new PersistException(e);
        }
    }

    public static Flat read(File folder) throws ReadException {
        try {
            File xmlFile = new File(folder, "flat.xml");
            Document doc = XmlUtil.readDocument(xmlFile);
            doc.getDocumentElement().normalize();
            Element root = doc.getDocumentElement();
            NodeList nodeList = root.getChildNodes();

            Flat flat = new Flat();

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node currentNode = nodeList.item(i);
                if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) currentNode;
                    String nodeContent = element.getTextContent().trim();
                    if(!nodeContent.isEmpty()) {
                        String classAsString = element.getTagName();
                        String name = element.getAttribute(NAME_XML_ATTR);
                        Object objectValue;
                        switch (classAsString) {
                            case "String":
                                objectValue = nodeContent;
                                break;
                            case "Float":
                                objectValue = new Float(nodeContent);
                                break;
                            case "Double":
                                objectValue = new Double(nodeContent);
                                break;
                            case "DateTime":
                                objectValue = ISODateTimeFormat.dateTime().parseDateTime(nodeContent);
                                break;
                            case "DateTimeWrapper":
                                objectValue = new DateTimeWrapper(ISODateTimeFormat.dateTime().parseDateTime(nodeContent));
                                break;
                            case "SiteType":
                                objectValue = SiteType.valueOf(nodeContent);
                                break;
                            default:
                                continue;
                        }
                        FlatValueRetriever<?> flatValueRetriever = FlatData.getRetriever(name);
                        if (flatValueRetriever == null) {
                            System.out.println("wtf");
                        }
                        Value<?> value = flatValueRetriever.retrieve(flat);
                        set(value, objectValue);
                    }
                }
            }
            return flat;
        } catch (IOException e) {
            throw new ReadException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private static <A> void set(Value<?> value, A objectValue){
        Value<A> castValue = (Value<A>) value;
        castValue.set(objectValue);
    }
}
