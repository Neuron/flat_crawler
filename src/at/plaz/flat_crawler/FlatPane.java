package at.plaz.flat_crawler;

import at.plaz.flat_crawler.crawler.SiteType;
import at.plaz.flat_crawler.filters.DateTimeWrapper;
import at.plaz.flat_crawler.value_retriever.FlatValueRetriever;
import com.google.maps.model.TravelMode;
import com.sun.javafx.scene.text.TextLayout;
import com.sun.javafx.tk.Toolkit;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import static com.google.maps.model.TravelMode.*;

/**
 * Created by Georg Plaz.
 */
public class FlatPane extends ScrollPane {
    private FlatData flatData;
    private HBox imageBox = new HBox();
    private ImageView[] slides;
    private int imageIndex = 0;

    public FlatPane(Flat flat, AddressEntry... addressEntries) {
        GridPane grid = new GridPane();
        grid.getColumnConstraints().add(new ColumnConstraints());
        widthProperty().addListener((observable, oldValue, newValue) -> {
            grid.setPrefWidth(newValue.doubleValue());
        });

        setFitToWidth(true);
        ColumnConstraints columnConstraints = new ColumnConstraints();
        columnConstraints.setFillWidth(true);
        columnConstraints.setHgrow(Priority.ALWAYS);
        grid.getColumnConstraints().add(columnConstraints);
        this.flatData = flat.getFlatData();
        grid.setHgap(10);
        grid.setVgap(10);
        setPadding(new Insets(25, 25, 25, 25));
        grid.setAlignment(Pos.CENTER);
        setContent(grid);
        setPrefSize(1200, 800);

        int verticalIndex = 0;
        imageBox.setAlignment(Pos.CENTER);
        grid.add(imageBox, 0, verticalIndex++, 2, 1);

        File typeFolder = new File(flatData.getSiteType().get().getShortName());
        File objectFolder = new File(typeFolder, flatData.getId().get());
        File imageFolder = new File(objectFolder, "images");
        File[] imageFiles = imageFolder.listFiles();
        if(imageFiles != null) {
            slides = new ImageView[imageFiles.length];
            for (int i = 0; i < imageFiles.length; i++) {
                try {
                    ImageView imageView = new ImageView();
                    imageView.setImage(new Image(new FileInputStream(imageFiles[i])));
                    slides[i] = imageView;
                    final int nextIndex = (i + 1) % imageFiles.length;
                    imageView.setOnMouseClicked(event -> show(slides[nextIndex]));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            show(slides[imageIndex]);
        }

        int methodIndex = 0;
        for (FlatValueRetriever<?> flatValueRetriever : FlatData.getFlatValueRetrievers()){
            Value v = flatValueRetriever.retrieve(flat);
            if(v.has()) {
                Class<?> valueType = flatValueRetriever.getType();
                String valueLabel = flatValueRetriever.getLabel();

                String text = null;
                if (valueType == String.class || valueType == Float.class || valueType == Double.class || valueType == SiteType.class) {
                    text = String.valueOf(v.get());
                }else if(valueType == DateTimeWrapper.class){
                    Value<DateTimeWrapper> value = v;
                    DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
                    text = fmt.print(value.get().getDateTime());
                }
                if(text != null) {
                    grid.add(new Label(valueLabel), 0, verticalIndex);
                    grid.add(createTextArea(text), 1, verticalIndex);
                    verticalIndex++;
                }
            }
            if (methodIndex++ == FlatData.ADDRESS_VALUE_VALUE_POS && flatData.getAddress().has()) {
                String address = flatData.getAddress().get();
                for (AddressEntry addressEntry : addressEntries){
                    for(TravelMode travelMode : new TravelMode[]{TRANSIT, BICYCLING}) {
                        Maps.TravelNode travelNode = Maps.getTravelTime(address, addressEntry.getAddress(), travelMode);
                        if(travelNode != null) {
                            String labelText = "Travel time to "+addressEntry.getLabel()+" by "+travelMode.name().toLowerCase();
                            grid.add(new Label(labelText), 0, verticalIndex);
                            String durationString = travelNode.getDuration().humanReadable;
                            grid.add(createTextArea(durationString), 1, verticalIndex++);
                        }
                    }
                }
            }
        }
    }

    private TextArea createTextArea(String text) {
        TextArea textArea = new TextArea();
        textArea.textProperty().addListener((observable, oldValue, newValue) -> updateHeight(textArea));
        textArea.widthProperty().addListener((observable, oldValue, newValue) -> updateHeight(textArea));
        textArea.maxWidthProperty().addListener((observable, oldValue, newValue) -> updateHeight(textArea));
        textArea.setText(text);
        textArea.setWrapText(true);
        textArea.setEditable(false);
        return textArea;
    }

    private void updateHeight(TextArea textInput){
        Font font = textInput.getFont();
        Text helper = new Text();
        helper.setText(textInput.getText());
        helper.setFont(font);
        helper.setWrappingWidth(textInput.getWidth());
        textInput.setPrefHeight(helper.getLayoutBounds().getHeight()+10);
        textInput.setMinHeight(helper.getLayoutBounds().getHeight()+10);
    }

    private void show(ImageView imageView) {
        imageBox.getChildren().clear();
        imageBox.getChildren().add(imageView);
    }

    public static class AddressEntry{
        private String label;
        private String address;

        public AddressEntry(String label, String address) {
            this.label = label;
            this.address = address;
        }

        public String getLabel() {
            return label;
        }

        public String getAddress() {
            return address;
        }

        @Override
        public String toString() {
            return "AddressEntry{" +
                    "label='" + label + '\'' +
                    ", address='" + address + '\'' +
                    '}';
        }
    }
}
