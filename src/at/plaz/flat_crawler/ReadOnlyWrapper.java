package at.plaz.flat_crawler;

/**
 * Created by Georg Plaz.
 */
public class ReadOnlyWrapper<A> implements ReadOnlyValue<A> {
    private Property<A> property;

    public ReadOnlyWrapper(Property<A> property){
        this.property = property;
    }

    @Override
    public A get() {
        return property.get();
    }

    @Override
    public boolean has() {
        return property.has();
    }

    @Override
    public void addValueListener(ValueChangedListener<A> valueChangedListener) {
        property.addValueListener(valueChangedListener);
    }

    @Override
    public void removeValueListener(ValueChangedListener<A> valueChangedListener) {
        property.removeValueListener(valueChangedListener);
    }

}
