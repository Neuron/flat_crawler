package at.plaz.flat_crawler;

import org.apache.commons.lang3.StringEscapeUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.LinkedHashSet;
import java.util.Set;

import static at.plaz.flat_crawler.MessageDisplay.DEBUG_TAG;
import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.FileVisitResult.TERMINATE;

/**
 * Created by Georg Plaz.
 */
public class Util {
    private static final String[] MONTHS = new String[]{"jänner", "februar", "märz", "april", "may", "juni", "juli", "august", "september", "oktober", "november", "dezember"};

    public static void deleteFileOrFolder(final Path path) throws IOException {
        Files.walkFileTree(path, new SimpleFileVisitor<Path>(){
            @Override public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs)
                    throws IOException {
                Files.delete(file);
                return CONTINUE;
            }

            @Override public FileVisitResult visitFileFailed(final Path file, final IOException e) {
                return handleException(e);
            }

            private FileVisitResult handleException(final IOException e) {
                e.printStackTrace(); // replace with more robust error handling
                return TERMINATE;
            }

            @Override public FileVisitResult postVisitDirectory(final Path dir, final IOException e)
                    throws IOException {
                if(e!=null)return handleException(e);
                Files.delete(dir);
                return CONTINUE;
            }
        });
    }

    public static DateTime cleanDateString(String dateString){
        String originalDateString = dateString;
        dateString = dateString.toLowerCase().trim();
        if(dateString.contains("sofort") || dateString.contains("jetzt") || dateString.contains("ab heute")){
            return DateTime.now().minusDays(1);
        }
        for (int i = 0; i < 12; i++) {
            for(String month : new String[]{MONTHS[i], MONTHS[i].substring(0, 3)+".", MONTHS[i].substring(0, 3)}) {
                dateString = dateString.replaceAll("\\s" + month, (i+1) + ".");
                dateString = dateString.replaceAll(month, (i+1) + ".");
            }
        }
        Set<DateTime> dates = new LinkedHashSet<>();
        for(String s : dateString.split("[^0-9.\\s]")){
            DateTime date = parseDDMMYYYY(s);
            if(date != null){
                dates.add(date);
            }
        }
        if(dates.isEmpty()){
            DateTime date = parseDDMMYYYY(dateString.replaceAll("[^0-9.]", ""));
            if(date != null){
                return date;
            }else{
                return DateTimeFormat.forPattern("dd.MM.yyyy").parseDateTime(originalDateString);
            }
        }else{
            return dates.iterator().next();
        }
    }

//    public static String unescape(String toUnEscape){
//        while (true){
//            int startIndex = toUnEscape.indexOf("\\");
//            if(startIndex == -1){
//                break;
//            }
//            toUnEscape = toUnEscape.substring(0, startIndex) + StringEscapeUtils.unescapeJava(toUnEscape.substring(startIndex, startIndex+6)) + toUnEscape.substring(startIndex+6, toUnEscape.length());
//        }
//    }

    private static DateTime parseDDMMYYYY(String s){
        s = s.replaceAll("[^0-9.]", "");
        try{
            try{
                return parseDDMMYY(s);
            }catch (IllegalArgumentException e){
                return DateTimeFormat.forPattern("dd.MM.yyyy").parseDateTime(s);
            }
        }catch (IllegalArgumentException e){
            return parseDDMM(s);
        }

    }

    private static DateTime parseDDMM(String s) {
        try{
            DateTime date = DateTimeFormat.forPattern("dd.MM.").parseDateTime(s);
            DateTime now = DateTime.now();
            if(date.getMonthOfYear() > now.getMonthOfYear() - 6){
                date = date.withYear(now.getYear());
            }else{
                date = date.withYear(now.getYear() + 1);
            }
            return date;
        }catch (IllegalArgumentException e2){
            return parseDDMMYY(s);
        }
    }

    public static void main(String... args){
        System.out.println(cleanDateString("01.01.2016"));
    }

    private static DateTime parseDDMMYY(String s) {
        try {
            return DateTimeFormat.forPattern("dd.MM.yy").parseDateTime(s);
        }catch (IllegalArgumentException e3){
            return null;
        }
    }

    public static String normalizeGermanNumberString(String numberString){
        String value = numberString.replaceAll("[^,0-9]", "");
        value = value.replace(",", ".");
        return value;
    }

    public static double parseGermanNumberString(String value){
        return Double.parseDouble(normalizeGermanNumberString(value));
    }

    public static double toPrice(String priceAsString){
        return parseGermanNumberString(cleanPrice(priceAsString));
    }

    public static String cleanPrice(String price){
        return price.replace("€", "").trim();
    }

    public static String saveIsolate(String content, String start, String end, MessageDisplay messageDisplay){
        try {
            return isolate(content, start, end);
        }catch (IsolateException e){
            messageDisplay.display(e.getMessage(), DEBUG_TAG);
            return null;
        }
    }

    public static String isolate(String content, String start, String end){
        int startIndex;
        if(start != null){
            startIndex = content.indexOf(start) + start.length();
            if(startIndex == -1){
                throw new IsolateException("Could not isolate String with start " + quote(start) + " and end " + quote(end) + " " +
                        "because start was not found.");
            }
        }else{
            startIndex = 0;
        }

        int endIndex;
        if(end != null){
            endIndex = content.indexOf(end, startIndex);
            if(endIndex == -1){
                throw new IsolateException("Could not isolate String with start " + quote(start)+" and end " + quote(end) + " " +
                        "because end was not found.");
            }
        }else{
            endIndex = content.length();
        }

        return content.substring(startIndex, endIndex);
    }

    public static String quote(String toQuote){
        if(toQuote != null){
            return "\""+toQuote+"\"";
        }else{
            return "null";
        }
    }

    public static Double saveIsolateDouble(String content, String start, String end, MessageDisplay messageDisplay){
        try {
            return isolateDouble(content, start, end);
        }catch (IsolateException e){
            messageDisplay.display(e.getMessage(), DEBUG_TAG);
            return null;
        }
    }

    public static Double isolateDouble(String content, String start, String end){
        String isolatedString = isolate(content, start, end);
        try{
            if(isolatedString != null){
                return Double.valueOf(isolatedString);
            }else{
                return null;
            }
        } catch (NumberFormatException e){
            throw new IsolateException("Could not parse \""+isolatedString+"\" to double.");
        }
    }

    public static Integer saveIsolateInt(String content, String start, String end, MessageDisplay messageDisplay){
        try {
            return isolateInt(content, start, end);
        }catch (IsolateException e){
            messageDisplay.display(e.getMessage(), DEBUG_TAG);
            return null;
        }
    }

    public static Integer isolateInt(String content, String start, String end){
        String isolatedString = isolate(content, start, end);
        try{
            if(isolatedString != null){
                return Integer.valueOf(isolatedString);
            }else{
                return null;
            }
        } catch (NumberFormatException e){
            throw new IsolateException("Could not parse \""+isolatedString+"\" to an integer.");
        }
    }

    public static class IsolateException extends RuntimeException{

        public IsolateException(String s) {
            super(s);
        }
    }
}
