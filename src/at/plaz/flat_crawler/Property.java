package at.plaz.flat_crawler;

/**
 * Created by Georg Plaz.
 */
public interface Property<A> {
    A get();

    boolean has();

    void addValueListener(ValueChangedListener<A> valueChangedListener);

    void removeValueListener(ValueChangedListener<A> valueChangedListener);
}
