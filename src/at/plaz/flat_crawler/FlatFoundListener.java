package at.plaz.flat_crawler;

/**
 * Created by Georg Plaz.
 */
public interface FlatFoundListener {
    void found(Flat flat);
}
