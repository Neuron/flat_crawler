package at.plaz.flat_crawler;

import at.plaz.flat_crawler.crawler.FlatCrawler;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

/**
 * Created by Georg Plaz.
 */
public class CrawlerPane extends GridPane{
    private ProgressBar refreshingInBar;
    private Label refreshingInLabel;
    private int verticalIndex = 0;
    private Value<Float> refreshingIn;
    private final SimplePaneDisplay display;

    public CrawlerPane(FlatCrawler flatCrawler){
        display = new SimplePaneDisplay();
        add(display, 0, verticalIndex++, 2, 1);

        ColumnConstraints columnConstraints = new ColumnConstraints();
        columnConstraints.setFillWidth(true);
        columnConstraints.setHgrow(Priority.ALWAYS);
        getColumnConstraints().add(columnConstraints);
        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setFillHeight(true);
        rowConstraints.setVgrow(Priority.ALWAYS);
        getRowConstraints().add(rowConstraints);
    }

    public void setRefreshingIn(Value<Float> refreshingIn) {
        this.refreshingIn = refreshingIn;
        refreshingIn.addValueListener((oldValue, newValue) -> Platform.runLater(() -> {
            if(newValue != null){
                refreshingInBar.setProgress(newValue);
            } else {
                refreshingInBar.setProgress(0);
            }
        }));
        refreshingInLabel = new Label("Refreshing in...");
        refreshingInBar = new ProgressBar();
        refreshingInBar.setProgress(1);

        add(refreshingInLabel, 0, verticalIndex, 1, 1);
        add(refreshingInBar, 1, verticalIndex++, 1, 1);
    }

    public SimplePaneDisplay getDisplay() {
        return display;
    }
}
